package com.pranisheba.sharedfarming.model
import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class UserLogin(
  var username: String?,
  var password: String?,
  var token: String?,
  var fcm_token : String?,
  var expires_in : String?,
) : Parcelable {
  constructor(
    username: String?,
    password: String?,
    fcm_token: String?
  ) : this(username, password, null,fcm_token,null
  )
}