package com.pranisheba.sharedfarming.util
import android.app.ActionBar
import android.content.Context
import android.graphics.Color
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.view.Gravity
import android.view.View
import android.widget.TextView
import android.widget.Toast
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.pranisheba.sharedfarming.R



//For language this code will be used
//fun setLocale(context: Context, language: String) {
//  val locale = Locale(language)
//  Locale.setDefault(locale)
//  val config = context.resources.configuration
//  config.setLocale(locale)
//  context.createConfigurationContext(config)
//  context.resources.updateConfiguration(config, context.resources.displayMetrics)
//}

// check network available or not
fun isNetworkAvailable(context: Context?): Boolean {
  if (context == null) return false
  val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
  if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
    val capabilities = connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
    if (capabilities != null) {
      when {
        capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> {
          return true
        }
        capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> {
          return true
        }
        capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> {
          return true
        }
      }
    }
  } else {
    val activeNetworkInfo = connectivityManager.activeNetworkInfo
    if (activeNetworkInfo != null && activeNetworkInfo.isConnected) {
      return true
    }
  }
  return false
}

// To show toast message
fun Context.toast(message: CharSequence) =
  Toast.makeText(this, message, Toast.LENGTH_SHORT).show()

fun showMsg(context: Context, msg : String){
  context.toast(msg)
}

// showSnackBar..
fun showSnackBar(view: View, textOfMessage : String) {
  val snackBar = Snackbar.make(view, textOfMessage, Snackbar.LENGTH_LONG)
  val snackBarView = snackBar.view
  val layoutParams = ActionBar.LayoutParams(snackBarView.layoutParams)
  layoutParams.gravity = Gravity.TOP
  layoutParams.setMargins(0,0,0,0)
  snackBarView.setPadding(0, 8, 0, 0)
  snackBarView.layoutParams = layoutParams
  snackBar.animationMode = BaseTransientBottomBar.ANIMATION_MODE_FADE
  val txt = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text) as TextView
  snackBarView.setBackgroundResource(R.drawable.snack_bar)
  txt.setTextColor(Color.WHITE)
  if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
    txt.textAlignment = View.TEXT_ALIGNMENT_CENTER;
  } else {
    txt.gravity = Gravity.CENTER_HORIZONTAL;
  }

  snackBar.show()
}

fun showNoInternetBar(view: View) {
  val snackBar = Snackbar.make(view, "No Internet !!  Check your connection ", Snackbar.LENGTH_LONG)
  val snackBarView = snackBar.view
  val layoutParams = ActionBar.LayoutParams(snackBarView.layoutParams)
  layoutParams.gravity = Gravity.TOP
  snackBarView.setPadding(0, 8, 0, 0)
  snackBarView.layoutParams = layoutParams
  snackBar.animationMode = BaseTransientBottomBar.ANIMATION_MODE_FADE
  val txt = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text) as TextView
  snackBarView.setBackgroundResource(R.drawable.snack_bar2)
  txt.setTextColor(Color.WHITE)
  if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
    txt.textAlignment = View.TEXT_ALIGNMENT_CENTER;
  } else {
    txt.gravity = Gravity.CENTER_HORIZONTAL;
  }
  snackBar.show()
}









