package com.pranisheba.sharedfarming.ui.activity

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.pranisheba.sharedfarming.R
import com.pranisheba.sharedfarming.databinding.ActivityProfileBinding
import com.pranisheba.sharedfarming.model.ProfileModel
import com.pranisheba.sharedfarming.preference.SharedFarmingPreference
import com.pranisheba.sharedfarming.ui.viewmodel.ProfileViewModel
import com.pranisheba.sharedfarming.util.*
import com.squareup.picasso.Picasso
import kotlinx.coroutines.launch

class ProfileActivity : AppCompatActivity() {

  private lateinit var binding: ActivityProfileBinding
  private lateinit var viewModel : ProfileViewModel
  private var profileInfo : ProfileModel ?= null

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    binding = ActivityProfileBinding.inflate(layoutInflater)
    setContentView(binding.root)
    viewModel = ViewModelProvider(this)[ProfileViewModel::class.java]
    supportActionBar?.setDisplayHomeAsUpEnabled(true)
    supportActionBar?.setDisplayShowHomeEnabled(true)
    supportActionBar?.setTitle(R.string.profile)
    if(!isNetworkAvailable(this)){
      showNoInternetBar(binding.bankingCard)
    }
    getProfileInfo()
    binding.personalCard.setOnClickListener {
      if(!isNetworkAvailable(this)){
        showMsg(this,"No Internet connection")
        return@setOnClickListener
      }
      val intent = Intent(this, PersonalInfoDetailsActivity::class.java)
      intent.putExtra(PROFILE_CONTACT_BASE, "all")
      startActivity(intent)
    }
    viewModel.progress.observe(this, {
      if (it) {
        binding.animationView.visibility = View.VISIBLE
      } else {
        binding.animationView.visibility = View.GONE
      }
    })
    viewModel.tokenRemove.observe(this, {
      if(it){
        showMsg(this, "Log In to view Info ..")
        SharedFarmingPreference(this@ProfileActivity).putAuthToken(null)
        startActivity(Intent(this@ProfileActivity,LoginActivity::class.java))
        finish()
      }
    })

    binding.contactCard.setOnClickListener {
      showMsg(this,"Please wait..")
      if(!isNetworkAvailable(this)){
        showMsg(this,"No Internet connection")
        return@setOnClickListener
      }
      val intent = Intent(this, PersonalInfoDetailsActivity::class.java)
      intent.putExtra(PROFILE_CONTACT_BASE, "contact")
      startActivity(intent)
    }
    binding.bankingCard.setOnClickListener {
      if(!isNetworkAvailable(this)){
        showMsg(this,"No Internet connection")
        return@setOnClickListener
      }
      if(profileInfo != null ){
        val intent = Intent(this@ProfileActivity,BankingDetailsActivity::class.java)
        intent.putExtra(PROFILE_INFO, profileInfo)
        startActivity(intent)
      }
      else{
        showMsg(this,"Please wait..")
      }
    }
    binding.nomineeCard.setOnClickListener {
      if(!isNetworkAvailable(this)){
        showMsg(this,"No Internet connection")
        return@setOnClickListener
      }
      if(profileInfo != null ){
        val intent = Intent(this@ProfileActivity,NomineeDetailsActivity::class.java)
        intent.putExtra(PROFILE_INFO, profileInfo)
        startActivity(intent)
      }
      else{
        showMsg(this,"Please wait..")
      }
    }
  }

  private fun getProfileInfo() {
    val token  = "token "+ SharedFarmingPreference(this@ProfileActivity).getAuthToken().toString()
    lifecycleScope.launch {
      viewModel.getProfile( token)
    }

    viewModel.userProfile.observe(this, { profileObjectList ->
      Log.d("Profile Info",profileObjectList.toString())
      if(profileObjectList != null){
        profileInfo = profileObjectList[0]
        binding.userNameText.text = SharedFarmingPreference(this).getAuthPhone()
        binding.userEmailText.text = SharedFarmingPreference(this).getAuthMail()
        val url = profileInfo?.profile_img.toString()
        //Loading Image into view
        Picasso.get()
          .load(url)
          .placeholder(R.drawable.ic_baseline_person_24)
          .error(R.drawable.ic_baseline_person_24)
          .into(binding.profileImage)
      }
      else{
        Log.d("Profile Info","List of object is null")
      }

    })

  }

  override fun onResume() {
    super.onResume()
    getProfileInfo()
  }
  override fun onOptionsItemSelected(item: MenuItem): Boolean {
    // handle arrow click here
    if (item.itemId == android.R.id.home) {
      onBackPressed()
    }
    return super.onOptionsItemSelected(item)
  }

  companion object {
    const val TAG = "Profile Activity"
  }
}
