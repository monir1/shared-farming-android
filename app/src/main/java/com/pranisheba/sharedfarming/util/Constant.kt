package com.pranisheba.sharedfarming.util

const val FUND_OPPORTUNITY = "fund-opportunity"
const val ALL_FUND_OPPORTUNITY = "fund-opportunity-list"
const val REGULAR_OPPORTUNITY = "regular_fund-opportunity-list"
const val SHARIAH_OPPORTUNITY = "shariah_fund-opportunity-list"
const val WHAT_TO_LOAD_FUND = "load-opportunity-list"

const val AUTH_TOKEN = "auth-token"
const val PROFILE_INFO = "profile-info"
const val EMAIL = "mail"
const val PHONE = "phone"
const val EXP_TIME ="exp-time"
const val INVOICE ="invoice"

const val UPDATE_CREATE_NOMINEE_INFO = "nominee-info"
const val UPDATE_CREATE_NOMINEE = "nominee-info-obj"
const val UPDATE_CREATE_BANK_INFO = "bank-info"
const val UPDATE_CREATE_BANK = "bank-info-obj"

const val PROFILE_CONTACT_BASE = "profile-info-contact-base"