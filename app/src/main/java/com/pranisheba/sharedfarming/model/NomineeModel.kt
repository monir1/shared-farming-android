package com.pranisheba.sharedfarming.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class NomineeModel (
  var id : Int?,
  var name : String?,
  var relationship : String?,
  var contact_no : String?,
  var nid: String?,
    ) : Parcelable