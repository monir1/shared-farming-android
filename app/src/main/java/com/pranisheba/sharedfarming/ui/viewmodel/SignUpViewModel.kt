package com.pranisheba.sharedfarming.ui.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.pranisheba.sharedfarming.model.UserSignUp
import com.pranisheba.sharedfarming.networking.ApiClient
import com.pranisheba.sharedfarming.networking.ApiInterface
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import java.io.IOException

class SignUpViewModel : ViewModel() {
  val apiClient = ApiClient().getApiClient()?.create(ApiInterface::class.java)
  private val _progress = MutableLiveData<Boolean>()
  val progress: LiveData<Boolean>
    get() = _progress

  private val _userSignUp = MutableLiveData<UserSignUp>()
  val userSignUp: LiveData<UserSignUp>
    get() = _userSignUp
  private val _progressRight = MutableLiveData<Boolean>()
  val progressRight: LiveData<Boolean>
    get() = _progressRight

  fun signUp(userSignUp: UserSignUp) {
    _progressRight.value = true
    _progress.value = true
    viewModelScope.launch {
      val response = try {
        withContext(Dispatchers.IO) {
          apiClient?.userSignUp(userSignUp)
        }
      } catch (e: IOException) {
        return@launch
      } catch (e: HttpException) {
        return@launch
      } catch (e: Exception) {
        return@launch
      }
      if (response?.isSuccessful == true && response.body() != null) {
        _userSignUp.value = response.body()
        _progress.value = false
      } else {
        _progress.value = false

        if (response?.code() == 400) {
          Log.d("doneValue 2 :: ", response.message())
          _progressRight.value = false
        } else if (response?.code() != null) {
          Log.d("doneValue :: ", response.message())
        }
      }
    }

  }
}
