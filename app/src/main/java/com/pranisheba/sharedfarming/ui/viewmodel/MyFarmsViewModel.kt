package com.pranisheba.sharedfarming.ui.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.pranisheba.sharedfarming.model.InvestmentSummery
import com.pranisheba.sharedfarming.model.Invoice
import com.pranisheba.sharedfarming.networking.ApiClient
import com.pranisheba.sharedfarming.networking.ApiInterface
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import java.io.IOException

class MyFarmsViewModel : ViewModel() {
  val apiClient = ApiClient().getApiClient()?.create(ApiInterface::class.java)

  private val _progress = MutableLiveData<Boolean>()
  val progress: LiveData<Boolean>
    get() = _progress

  private val _invoices = MutableLiveData<List<Invoice>>()
  val invoices: LiveData<List<Invoice>>
    get() = _invoices

  private val _tokenRemove = MutableLiveData<Boolean>()
  val tokenRemove: LiveData<Boolean>
    get() = _tokenRemove

  fun getInvoices(token: String) {
    _progress.value = true
    _tokenRemove.value = false
    viewModelScope.launch {

      val response = try {
        withContext(Dispatchers.IO) {
          apiClient?.getInvoices(token)
        }
      } catch (e: IOException) {
        return@launch
      } catch (e: HttpException) {
        return@launch
      } catch (e: Exception) {
        return@launch
      }
      if (response?.isSuccessful == true && response.body() != null) {
        _invoices.value = response.body()
        _progress.value = false
      } else {
        _progress.value = false
        if (response?.code() == 401) {
          Log.d("doneValue :: ", response.message())
        }
      }
    }
  }

  private val _invoicesListObj = MutableLiveData<InvestmentSummery>()
  val invoicesListObj: LiveData<InvestmentSummery>
    get() = _invoicesListObj
   fun getInvoicesItemObj(token: String) {
    _tokenRemove.value = false
    _progress.value = true
    viewModelScope.launch {
      val apiClient = ApiClient().getApiClient()?.create(ApiInterface::class.java)
      val response = try {
        withContext(Dispatchers.IO) {
          apiClient?.getInvestSummery(token)
        }
      } catch (e: IOException) {
        return@launch
      } catch (e: HttpException) {
        return@launch
      } catch (e: Exception) {
        return@launch
      }
      if (response?.isSuccessful == true && response.body() != null) {
        _invoicesListObj.value = response.body()
        _progress.value = false
      } else {
        _progress.value = false
        if (response?.code() == 401) {
          Log.d("doneValue :: ", response.message())
          _tokenRemove.value = true
        }
      }
    }
  }
}