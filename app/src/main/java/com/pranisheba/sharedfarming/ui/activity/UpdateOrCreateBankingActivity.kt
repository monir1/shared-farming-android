package com.pranisheba.sharedfarming.ui.activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.lifecycle.ViewModelProvider
import com.pranisheba.sharedfarming.R
import com.pranisheba.sharedfarming.databinding.ActivityUpdateOrCreateBankingBinding
import com.pranisheba.sharedfarming.model.*
import com.pranisheba.sharedfarming.preference.SharedFarmingPreference
import com.pranisheba.sharedfarming.ui.viewmodel.BankingViewModel
import com.pranisheba.sharedfarming.util.*

class UpdateOrCreateBankingActivity : AppCompatActivity() {
  private lateinit var binding: ActivityUpdateOrCreateBankingBinding
  private lateinit var viewModel : BankingViewModel
  private lateinit var bankCreate : String
  private lateinit var banking : BankingModel
  private lateinit var personalDetails: ProfileModel
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    binding = ActivityUpdateOrCreateBankingBinding.inflate(layoutInflater)
    setContentView(binding.root)
    viewModel = ViewModelProvider(this)[BankingViewModel::class.java]
    supportActionBar?.setDisplayHomeAsUpEnabled(true)
    supportActionBar?.setDisplayShowHomeEnabled(true)
    personalDetails = intent.getParcelableExtra(PROFILE_INFO)!!
    bankCreate = intent.getStringExtra(UPDATE_CREATE_BANK_INFO)!!

    if(bankCreate == "create"){
      supportActionBar?.setTitle(R.string.createBanking)
      bankingCreate()
    }
    else{
      supportActionBar?.setTitle(R.string.updateBank)
      banking = intent.getParcelableExtra(UPDATE_CREATE_BANK)!!
      updateBanking()
    }

    viewModel.postBanking.observe(this,  {
      if(it != null){
        showMsg(this,"Info Updated Successfully")
        val intent = Intent(this,BankingDetailsActivity::class.java)
        intent.putExtra(PROFILE_INFO,personalDetails)
        startActivity(intent)
        finish()
      }
    })
    viewModel.updateBanking.observe(this,  {
      if(it != null){
        showMsg(this,"Info Updated Successfully")
        val intent = Intent(this,BankingDetailsActivity::class.java)
        intent.putExtra(PROFILE_INFO,personalDetails)
        startActivity(intent)
        finish()
      }
    })
    viewModel.progress.observe(this, {
      if (it) {
        binding.animationView.visibility = View.VISIBLE
      } else {
        binding.animationView.visibility = View.GONE
      }
    })

    viewModel.tokenRemove.observe(this, {
      if(it){
        showMsg(this, "Log In to view Info")
        SharedFarmingPreference(this).putAuthToken(null)
        startActivity(Intent(this,LoginActivity::class.java))
        finish()
      }
    })
  }

  private fun updateBanking() {
    binding.itemDescriptionName.editText?.setText(banking.name)
    binding.itemDescriptionBranchName.editText?.setText(banking.branch_name)
    binding.itemDescriptionAccountNo.editText?.setText(banking.account_no)
    binding.itemDescriptionAccountName.editText?.setText(banking.Account_name)
    binding.submitData.setOnClickListener {
      updateBankInfo()
    }
  }

  private fun updateBankInfo() {
    val token = "token " + SharedFarmingPreference(this).getAuthToken().toString()
    val name = binding.itemDescriptionName.editText?.text.toString().trim()
    val branchName = binding.itemDescriptionBranchName.editText?.text.toString().trim()
    val acccount = binding.itemDescriptionAccountNo.editText?.text.toString().trim()
    val accountName = binding.itemDescriptionAccountName.editText?.text.toString().trim()

    if(name.isEmpty()){
      binding.itemDescriptionName.error = getString(R.string.thisIsMust)
      binding.itemDescriptionName.requestFocus()
      return
    }
    if(branchName.isEmpty()){
      binding.itemDescriptionBranchName.error = getString(R.string.thisIsMust)
      binding.itemDescriptionBranchName.requestFocus()
      return
    }
    if(acccount.isEmpty()){
      binding.itemDescriptionAccountNo.error = getString(R.string.thisIsMust)
      binding.itemDescriptionAccountNo.requestFocus()
      return
    }
    if(accountName.isEmpty()){
      binding.itemDescriptionAccountName.error = getString(R.string.thisIsMust)
      binding.itemDescriptionAccountName.requestFocus()
      return
    }
    if(name.isNotEmpty() && branchName.isNotEmpty() && acccount.isNotEmpty() && accountName.isNotEmpty()){
      viewModel.updateBanking(token, PersonalBankingInfoSubmitModel(name,branchName,acccount,accountName),banking.id!!)
    }
  }

  private fun bankingCreate() {
    binding.submitData.setOnClickListener {
      createBankInfo()
    }
  }

  private fun createBankInfo() {
    val token = "token " + SharedFarmingPreference(this).getAuthToken().toString()
    val name = binding.itemDescriptionName.editText?.text.toString().trim()
    val branchName = binding.itemDescriptionBranchName.editText?.text.toString().trim()
    val acccount = binding.itemDescriptionAccountNo.editText?.text.toString().trim()
    val accountName = binding.itemDescriptionAccountName.editText?.text.toString().trim()

    if(name.isEmpty()){
      binding.itemDescriptionName.error = getString(R.string.thisIsMust)
      binding.itemDescriptionName.requestFocus()
      return
    }
    if(branchName.isEmpty()){
      binding.itemDescriptionBranchName.error = getString(R.string.thisIsMust)
      binding.itemDescriptionBranchName.requestFocus()
      return
    }
    if(acccount.isEmpty()){
      binding.itemDescriptionAccountNo.error = getString(R.string.thisIsMust)
      binding.itemDescriptionAccountNo.requestFocus()
      return
    }
    if(accountName.isEmpty()){
      binding.itemDescriptionAccountName.error = getString(R.string.thisIsMust)
      binding.itemDescriptionAccountName.requestFocus()
      return
    }
    if(name.isNotEmpty() && branchName.isNotEmpty() && acccount.isNotEmpty() && accountName.isNotEmpty()){
      viewModel.postBanking(token, PersonalBankingInfoSubmitModel(name,branchName,acccount,accountName))
    }
  }

  override fun onOptionsItemSelected(item: MenuItem): Boolean {
    // handle arrow click here
    if (item.itemId == android.R.id.home) {
      onBackPressed()
    }
    return super.onOptionsItemSelected(item)
  }

  companion object {
    const val TAG = "Banking Create Update Activity"
  }
}