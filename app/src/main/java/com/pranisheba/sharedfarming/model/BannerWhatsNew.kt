package com.pranisheba.sharedfarming.model

import android.os.Parcelable
import com.denzcoskun.imageslider.models.SlideModel
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize


@Parcelize
data class BannerWhatsNew(
  @SerializedName("id") var id: Int? = null,
  @SerializedName("created") var created: String? = null,
  @SerializedName("modified") var modified: String? = null,
  @SerializedName("image") var image: String? = null,
  @SerializedName("is_active") var isActive: Boolean? = null,
  @SerializedName("product") var product: Int? = null,
) : Parcelable



