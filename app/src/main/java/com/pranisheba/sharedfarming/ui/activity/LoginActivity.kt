package com.pranisheba.sharedfarming.ui.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.util.Patterns
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.util.PatternsCompat
import androidx.lifecycle.ViewModelProvider

import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.tasks.*
import com.google.firebase.messaging.FirebaseMessaging
import com.pranisheba.sharedfarming.R
import com.pranisheba.sharedfarming.databinding.ActivityLoginBinding

import com.pranisheba.sharedfarming.model.UserLogin
import com.pranisheba.sharedfarming.preference.SharedFarmingPreference
import com.pranisheba.sharedfarming.ui.viewmodel.LoginViewModel
import com.pranisheba.sharedfarming.util.isNetworkAvailable
import com.pranisheba.sharedfarming.util.showMsg
import com.pranisheba.sharedfarming.util.showNoInternetBar
import com.pranisheba.sharedfarming.util.showSnackBar



class LoginActivity : AppCompatActivity() {
  private lateinit var tokenFcm : String
  private lateinit var binding: ActivityLoginBinding
  private lateinit var loginViewModel: LoginViewModel
  private lateinit var preference: SharedFarmingPreference
  private  var mail = ""
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    binding = ActivityLoginBinding.inflate(layoutInflater)
    setContentView(binding.root)
    supportActionBar?.setDisplayHomeAsUpEnabled(true)
    supportActionBar?.setDisplayShowHomeEnabled(true)
    supportActionBar?.title = getString(R.string.log_in)
    if(!isNetworkAvailable(this)){
      showNoInternetBar(binding.usernameLayout)
    }
      if(checkGooglePlayServices()){
        getFcmToken()
      }
      else tokenFcm = "not supported"



    loginViewModel = ViewModelProvider(this)[LoginViewModel::class.java]
    preference = SharedFarmingPreference(this)

    binding.loginButton.setOnClickListener {
      login()
    }

    binding.signUpTextView.setOnClickListener {
      goToSignUp()
    }

    binding.forgetPassTextView.setOnClickListener {
      goToForgetPass()
    }

    loginViewModel.userLogin.observe(this, {
      if (it != null) {
         showMsg(this,"Log in successfully")
        preference.putAuthToken(it.token.toString())
        preference.putAuthPhone(it.username)
        preference.putAuthMail(mail)
        preference.putExpTokenTime(it.expires_in.toString())
        startActivity(Intent(this, MainActivity::class.java))
        finishAffinity()
      }
    })

    loginViewModel.progress.observe(this, {
      if (it) {
        binding.animationView.visibility = View.VISIBLE
      } else {
        binding.animationView.visibility = View.GONE
      }
    })

    loginViewModel.progressRight.observe(this,{
      if (!it) {
        showSnackBar(binding.usernameLayout,"Give Username and pass properly")
      }
    })
  }

  private fun goToForgetPass() {
    startActivity(Intent(this, ForgetPasswordActivity::class.java))
  }

  private fun login() {
    try {
      val imm: InputMethodManager =
        getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
      imm.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
    } catch (e: java.lang.Exception) {

    }
    val username = binding.usernameLayout.editText?.text.toString()
    val password = binding.passwordLayout.editText?.text.toString()

    if (username.isEmpty()) {
      binding.usernameLayout.error = getString(R.string.phone_number_or_email_required)
      return
    }
    if (password.isEmpty()) {
      binding.passwordLayout.error = getString(R.string.password_required)
      return
    }


    if(!isEmailValid(username) && !isValidCellPhone(username)){
      binding.usernameLayout.error = getString(R.string.valid_phone_number_or_email_required)
      return
    }

    if(isValidCellPhone(username)){
      val phone = username
      if(phone.length != 11){
        binding.usernameLayout.error = getString(R.string.valid_phone_number_required)
        return
      }
      if(phone.substring(0,2) != "01"){
        binding.usernameLayout.error = getString(R.string.valid_phone_number_required)
        return
      }
    }


    mail = if(Patterns.EMAIL_ADDRESS.matcher(username).matches()){
      username
    } else{
      ""
    }


    val userLogin = UserLogin(username, password,tokenFcm )
    loginViewModel.login(userLogin)
  }

  private fun goToSignUp() {
    startActivity(Intent(this, SignUpActivity::class.java))
  }

  override fun onOptionsItemSelected(item: MenuItem): Boolean {
    // handle arrow click here
    if (item.itemId == android.R.id.home) {
      onBackPressed()
    }
    return super.onOptionsItemSelected(item)
  }

  companion object {
    const val TAG = "LoginActivity"
    const val TAG2 = "FCM TOKEN :: "
  }


  fun getFcmToken() {

    FirebaseMessaging.getInstance().token.addOnSuccessListener { token: String ->
      try{
        if (!TextUtils.isEmpty(token)) {
          Log.d(TAG2, "retrieve token successful : $token")
          tokenFcm = token
        } else {
          Log.w(TAG2, "token should not be null...")
        }
      }
      catch (e : Exception){
        Log.v(
          TAG2,
          "Exception of  :: $e"
        )
      }

    }.addOnFailureListener { _: Exception? -> }.addOnCanceledListener {

    }
      .addOnCompleteListener { task: Task<String> ->
       try{
         if(task.isSuccessful && task.result != null){

           Log.v(
             TAG2,
             "This is the token : " + task.result
           )
           tokenFcm = task.result.toString()
         }
         else{
           Log.v(
             TAG2,
             "Task is not successful"
           )
         }
       }
       catch (e : Exception){
         Log.v(
           TAG2,
           "Exception of  :: $e"
         )
       }
      }
      .addOnCanceledListener {

      }


  }

  private fun checkGooglePlayServices(): Boolean {
    val availability = GoogleApiAvailability.getInstance()
    val resultCode = availability.isGooglePlayServicesAvailable(this)
    if (resultCode != ConnectionResult.SUCCESS) {
      availability.getErrorDialog(this, resultCode, 0)?.show()
      return false
    }
    return true
  }


  private fun isValidCellPhone(number: String?): Boolean {
    return Patterns.PHONE.matcher(number).matches()
  }
  private fun isEmailValid(email: String): Boolean {
    return PatternsCompat.EMAIL_ADDRESS.matcher(email).matches()
  }

}