package com.pranisheba.sharedfarming.ui.adapter
import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.pranisheba.sharedfarming.R
import com.pranisheba.sharedfarming.model.FAQCheckout

class FaqAdapter(
  private val list: List<FAQCheckout>,
) :
  RecyclerView.Adapter<FaqAdapter.MyViewHolder>() {

  class MyViewHolder(view: View) :
    RecyclerView.ViewHolder(view) {
    val textViewQuestion : TextView? = view.findViewById(R.id.textViewQuestion)
    val textViewAns : TextView? = view.findViewById(R.id.textViewAnswer)
    val layoutCons : ConstraintLayout? = view.findViewById(R.id.layoutCons)
  }

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
    val itemView: View = LayoutInflater
      .from(parent.context)
      .inflate(
        R.layout.faq_item_layout,
        parent,
        false
      )
    return MyViewHolder(itemView)
  }

  @SuppressLint("SetTextI18n")
  override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
    val listData = list[position]
    holder.textViewQuestion?.text = listData.question
    holder.textViewAns?.text = listData.answer

    holder.layoutCons?.visibility = View.GONE
    holder.itemView.setOnClickListener {
      if(holder.layoutCons?.isVisible == true){
        holder.layoutCons.visibility = View.GONE
      }
      else{
        holder.layoutCons?.visibility = View.VISIBLE
      }

    }
  }

  override fun getItemCount(): Int {
    return list.size
  }

}