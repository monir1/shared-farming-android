package com.pranisheba.sharedfarming.ui.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.pranisheba.sharedfarming.R
import com.pranisheba.sharedfarming.model.FundOpportunity
import com.squareup.picasso.Picasso
import kotlin.math.roundToInt


class FundAdapter(
  private val list: List<FundOpportunity>,
  private val onItemClicked: (position: Int) -> Unit
) :
  RecyclerView.Adapter<FundAdapter.MyViewHolder>() {

  class MyViewHolder(view: View, private val onItemClicked: (position: Int) -> Unit) :
    RecyclerView.ViewHolder(view), View.OnClickListener {
    private val fundCard: CardView = view.findViewById(R.id.fundCardView)
    val image: ImageView = view.findViewById(R.id.image)
    val name: TextView = view.findViewById(R.id.name)
    val price: TextView = view.findViewById(R.id.price)
    val description : TextView = view.findViewById(R.id.descriptionText)
    val location : TextView = view.findViewById(R.id.locationTextView)

    init {
      fundCard.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
      onItemClicked(adapterPosition)
    }
  }

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
    val itemView: View = LayoutInflater
      .from(parent.context)
      .inflate(
        R.layout.recycler_item_fund,
        parent,
        false
      )
    return MyViewHolder(itemView, onItemClicked)
  }

  @SuppressLint("SetTextI18n")
  override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
    val listData = list[position]
    //Loading Image into view
    Picasso.get().load(listData.image).placeholder(R.drawable.ic_baseline_image_24)
      .error(R.drawable.ic_baseline_broken_image_24).into(holder.image)
    holder.name.text = listData.name
    if(listData.amount != null ){
      if(listData.amount!! >1000.00){
        val v =  listData.amount!!/1000.00
        holder.price.text = v.roundToInt().toString() + "K ৳/unit"
      }
    }

    holder.location.text = listData.location?.name + "( ${listData.location?.bn_name} )"
    if(listData.profit_percentage != null){
      holder.description.text = "${listData.duration} months contract period with ${listData.profit_percentage}% return on funding"
    }
    else{
      holder.description.text = "${listData.duration} months contract period with ${listData.shariah_profit_from}% to ${listData.shariah_profit_to}% Profit Sharing"
    }

    if(position == list.size -1){
      val params = holder.itemView.layoutParams as RecyclerView.LayoutParams
      params.rightMargin = 16
      holder.itemView.layoutParams = params
    }
  }

  override fun getItemCount(): Int {
    return list.size
  }

}