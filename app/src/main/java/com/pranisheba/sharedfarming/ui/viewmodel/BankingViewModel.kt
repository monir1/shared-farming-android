package com.pranisheba.sharedfarming.ui.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.pranisheba.sharedfarming.model.BankingModel
import com.pranisheba.sharedfarming.model.NomineeModel
import com.pranisheba.sharedfarming.model.PersonalBankingInfoSubmitModel
import com.pranisheba.sharedfarming.model.SubmitNomineeInfoModel
import com.pranisheba.sharedfarming.networking.ApiClient
import com.pranisheba.sharedfarming.networking.ApiInterface
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import java.io.IOException

class BankingViewModel : ViewModel() {
  val apiClient = ApiClient().getApiClient()?.create(ApiInterface::class.java)

  private val _progress = MutableLiveData<Boolean>()
  val progress: LiveData<Boolean>
    get() = _progress

  private val _banking = MutableLiveData<List<BankingModel>>()
  val banking: LiveData<List<BankingModel>>
    get() = _banking

  private val _postBanking = MutableLiveData<BankingModel>()
  val postBanking: LiveData<BankingModel>
    get() = _postBanking

  private val _updateBanking = MutableLiveData<BankingModel>()
  val updateBanking: LiveData<BankingModel>
    get() = _updateBanking

  private val _tokenRemove = MutableLiveData<Boolean>()
  val tokenRemove: LiveData<Boolean>
    get() = _tokenRemove


  fun getBanking(token: String) {
    _tokenRemove.value = false
    _progress.value = true
    viewModelScope.launch {
      val response = try {
        withContext(Dispatchers.IO) {
          apiClient?.getBankInfo(token)
        }
      } catch (e: IOException) {
        return@launch
      } catch (e: HttpException) {
        return@launch
      } catch (e: Exception) {
        Log.d("tag", e.toString())
        return@launch
      }
      if (response?.isSuccessful == true && response.body() != null) {
        _banking.value = response.body()
        _progress.value = false
      } else {
        _progress.value = false
        if (response?.code() == 401) {
          Log.d("doneValue :: ", response.message())
          _tokenRemove.value = true
        }
      }
    }
  }

  fun postBanking(token: String, bankInfo: PersonalBankingInfoSubmitModel) {
    _tokenRemove.value = false
    _progress.value = true
    viewModelScope.launch {
      val response = try {
        withContext(Dispatchers.IO) {
          apiClient?.postBankInfo(token, bankInfo)
        }
      } catch (e: IOException) {
        return@launch
      } catch (e: HttpException) {
        return@launch
      } catch (e: Exception) {
        Log.d("tag", e.toString())
        return@launch
      }
      if (response?.isSuccessful == true && response.body() != null) {
        _postBanking.value = response.body()
        _progress.value = false
      } else {
        _progress.value = false
        if (response?.code() == 401) {
          Log.d("doneValue :: ", response.message())
          _tokenRemove.value = true
        }
      }
    }
  }

  fun updateBanking(token: String, bankInfo: PersonalBankingInfoSubmitModel, bankId: Int) {
    _tokenRemove.value = false
    _progress.value = true
    viewModelScope.launch {
      val response = try {
        withContext(Dispatchers.IO) {
          apiClient?.updateBankInfo(token, bankInfo, bankId)
        }
      } catch (e: IOException) {
        return@launch
      } catch (e: HttpException) {
        return@launch
      } catch (e: Exception) {
        Log.d("tag", e.toString())
        return@launch
      }
      if (response?.isSuccessful == true && response.body() != null) {
        _updateBanking.value = response.body()
        _progress.value = false
      } else {
        _progress.value = false
        if (response?.code() == 401) {
          Log.d("doneValue :: ", response.message())
          _tokenRemove.value = true
        }
      }
    }
  }
}