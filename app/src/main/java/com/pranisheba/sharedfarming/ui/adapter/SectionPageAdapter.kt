package com.pranisheba.sharedfarming.ui.adapter
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.pranisheba.sharedfarming.ui.fragment.DescriptionOfItemFragment
import com.pranisheba.sharedfarming.ui.fragment.FAQFragment
import com.pranisheba.sharedfarming.ui.fragment.HowItWorksItem

class SectionPageAdapter (fm: FragmentManager, lifecycle : Lifecycle)
  : FragmentStateAdapter(fm, lifecycle) {

  override fun getItemCount(): Int {
    return 3
  }

  override fun createFragment(position: Int): Fragment {
    return when (position) {
      0 -> {
        DescriptionOfItemFragment()
      }
      1 -> {
        HowItWorksItem()

      }
      2 -> {
        FAQFragment()

      }
      else -> {
        Fragment()
      }
    }
  }


}
