package com.pranisheba.sharedfarming.ui.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Patterns
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.util.PatternsCompat
import androidx.lifecycle.ViewModelProvider
import com.pranisheba.sharedfarming.R
import com.pranisheba.sharedfarming.databinding.ActivitySignUpBinding
import com.pranisheba.sharedfarming.model.UserSignUp
import com.pranisheba.sharedfarming.ui.viewmodel.SignUpViewModel
import com.pranisheba.sharedfarming.util.isNetworkAvailable
import com.pranisheba.sharedfarming.util.showMsg
import com.pranisheba.sharedfarming.util.showNoInternetBar
import com.pranisheba.sharedfarming.util.showSnackBar

class SignUpActivity : AppCompatActivity() {

  private lateinit var binding: ActivitySignUpBinding
  private lateinit var signUpViewModel: SignUpViewModel

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    binding = ActivitySignUpBinding.inflate(layoutInflater)
    signUpViewModel = ViewModelProvider(this)[SignUpViewModel::class.java]
    setContentView(binding.root)
    supportActionBar?.setDisplayHomeAsUpEnabled(true)
    supportActionBar?.setDisplayShowHomeEnabled(true)
    supportActionBar?.title = getString(R.string.signup)
    if(!isNetworkAvailable(this)){
      showNoInternetBar(binding.phoneLayout)
    }

    binding.signUpButton.setOnClickListener {
      signUp()
    }

    binding.loginTextView.setOnClickListener {
      goToLogin()
    }

    signUpViewModel.userSignUp.observe(this, {
      if (it != null) {
        showMsg(this, "Signup successful, please Login")
        startActivity(Intent(this,LoginActivity::class.java))
        finish()
      }
    })

    signUpViewModel.progress.observe(this, {
      if (it) {
        binding.animationView.visibility = View.VISIBLE
      } else {
        binding.animationView.visibility = View.GONE
      }
    })

    signUpViewModel.progressRight.observe(this,{
        if (!it) {
          showSnackBar(binding.phoneLayout,"User is already registered")
        }
      })
  }

  private fun signUp() {
    try {
      val imm: InputMethodManager =
        getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
      imm.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
    } catch (e: java.lang.Exception) {

    }
    val phone = binding.phoneLayout.editText?.text.toString()
    val email = binding.emailLayout.editText?.text.toString()
    val password = binding.passwordLayout.editText?.text.toString()
    val retypePassword = binding.retypePasswordLayout.editText?.text.toString()

    if (phone.isEmpty()) {
      binding.phoneLayout.error = getString(R.string.phone_number_required)
      return
    }

    if(phone.isNotEmpty()){
      if(phone.length != 11){
        binding.phoneLayout.error = getString(R.string.valid_phone_number_required)
        return
      }
      if(!isValidCellPhone(phone)){
        binding.phoneLayout.error = getString(R.string.valid_phone_number_required)
        return
      }
      if(phone.substring(0,2) != "01"){
        binding.phoneLayout.error = getString(R.string.valid_phone_number_required)
        return
      }

    }
    if(email.isEmpty()){
      binding.emailLayout.error = getString(R.string.email_required)
      return
    }

    if(email.isNotEmpty()){
      if(!isEmailValid(email)){
        binding.phoneLayout.error = getString(R.string.valid_mail_required)
        return
      }
    }
    if (password.isEmpty()) {
      binding.passwordLayout.error = getString(R.string.password_required)
      return
    }
    if (retypePassword.isEmpty()) {
      binding.retypePasswordLayout.error = getString(R.string.retype_password_required)
      return
    }
    if (!binding.termsCheckBox.isChecked) {
      Toast.makeText(this, getString(R.string.accept_terms_and_conditions), Toast.LENGTH_SHORT)
        .show()
      return
    }
    if (password != retypePassword) {
      Toast.makeText(this, getString(R.string.password_did_not_match), Toast.LENGTH_SHORT)
        .show()
      return
    }

    val userSignUp = UserSignUp(phone, password, is_investor = true, is_agreed = true)
    if (email.isNotEmpty()) {
      userSignUp.email = email
    }

      signUpViewModel.signUp(userSignUp)


  }

  private fun goToLogin() {
    onBackPressed()
  }

  override fun onOptionsItemSelected(item: MenuItem): Boolean {
    // handle arrow click here
    if (item.itemId == android.R.id.home) {
      onBackPressed()
    }
    return super.onOptionsItemSelected(item)
  }

  companion object {
    const val TAG = "SignUpActivity"
  }

  private fun isValidCellPhone(number: String?): Boolean {
    return Patterns.PHONE.matcher(number).matches()
  }
  private fun isEmailValid(email: String): Boolean {
    return PatternsCompat.EMAIL_ADDRESS.matcher(email).matches()
  }
}
