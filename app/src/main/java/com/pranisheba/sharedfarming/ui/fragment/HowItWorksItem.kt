package com.pranisheba.sharedfarming.ui.fragment

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.pranisheba.sharedfarming.R
import com.pranisheba.sharedfarming.databinding.FragmentHowItWorksItemBinding
import com.pranisheba.sharedfarming.model.HelpListItem
import com.pranisheba.sharedfarming.ui.adapter.HelpItemAdapter
import com.pranisheba.sharedfarming.util.isNetworkAvailable
import com.pranisheba.sharedfarming.util.showMsg
import com.squareup.picasso.Picasso

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [HowItWorksItem.newInstance] factory method to
 * create an instance of this fragment.
 */
class HowItWorksItem : Fragment() {
  // TODO: Rename and change types of parameters
  private var param1: String? = null
  private var param2: String? = null
  private lateinit var binding: FragmentHowItWorksItemBinding
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    arguments?.let {
      param1 = it.getString(ARG_PARAM1)
      param2 = it.getString(ARG_PARAM2)
    }
  }

  @SuppressLint("NotifyDataSetChanged")
  override fun onCreateView(
    inflater: LayoutInflater, container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View {
    // Inflate the layout for this fragment
    binding = FragmentHowItWorksItemBinding.inflate(inflater, container, false)
    binding.helpRecycle.layoutManager =  LinearLayoutManager(requireContext()
      , LinearLayoutManager.VERTICAL, false)
    if(!isNetworkAvailable(requireContext())){
      showMsg(requireContext(),"No Internet connection")
    }
    Picasso.get().load("https://jouthokhamar.pranisheba.com.bd/media/section_with_image_background/Contract_Farming_Life_Cycle-1.png").placeholder(R.drawable.ic_baseline_image_24)
      .error(R.drawable.ic_baseline_broken_image_24).into(binding.howWorks)
    val listOfHelp  = ArrayList<HelpListItem>()
    listOfHelp.add(HelpListItem(getString(R.string.step1Name),getString(R.string.step1Invest)))
    listOfHelp.add(HelpListItem(getString(R.string.step2Name),getString(R.string.step2Invest)))
    listOfHelp.add(HelpListItem(getString(R.string.step3Name),getString(R.string.step3Invest)))
    listOfHelp.add(HelpListItem(getString(R.string.step4Name),getString(R.string.step4Invest)))
    listOfHelp.add(HelpListItem(getString(R.string.step5Name),getString(R.string.step5Invest)))
     loadHelp(listOfHelp)
    return binding.root
  }

  @SuppressLint("NotifyDataSetChanged")
  private fun loadHelp(listOfHelp : ArrayList<HelpListItem>) {
    val adapter = HelpItemAdapter(listOfHelp)
    binding.helpRecycle.adapter = adapter
    adapter.notifyDataSetChanged()
  }


  override fun onDestroyView() {
    super.onDestroyView()
  }
  companion object {
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HowItWorksItem.
     */
    // TODO: Rename and change types and number of parameters
    @JvmStatic
    fun newInstance(param1: String, param2: String) =
      HowItWorksItem().apply {
        arguments = Bundle().apply {
          putString(ARG_PARAM1, param1)
          putString(ARG_PARAM2, param2)
        }
      }
  }
}