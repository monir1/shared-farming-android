package com.pranisheba.sharedfarming.ui.fragment

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pranisheba.sharedfarming.R
import com.pranisheba.sharedfarming.databinding.FragmentDescriptionOfItemBinding
import android.content.Context
import com.pranisheba.sharedfarming.model.FundOpportunity
import com.pranisheba.sharedfarming.ui.activity.FundDetailsActivity
import com.pranisheba.sharedfarming.util.isNetworkAvailable
import com.pranisheba.sharedfarming.util.showMsg


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [DescriptionOfItemFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class DescriptionOfItemFragment : Fragment() {
  // TODO: Rename and change types of parameters
  private var param1: String? = null
  private var param2: String? = null
  private lateinit var binding: FragmentDescriptionOfItemBinding
  var fundOpportunity: FundOpportunity ? = null
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    arguments?.let {
      param1 = it.getString(ARG_PARAM1)
      param2 = it.getString(ARG_PARAM2)
    }
  }

  @SuppressLint("SetTextI18n")
  override fun onCreateView(
    inflater: LayoutInflater, container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View {

    binding = FragmentDescriptionOfItemBinding.inflate(inflater, container, false)
    binding.fundNameTextView.text = fundOpportunity?.name
    binding.fundAmountTextView.text = String.format("%.2f/cow", fundOpportunity?.amount)
    binding.breedTextView.text = fundOpportunity?.breed?.name
    binding.genderTextView.text = fundOpportunity?.gender
    binding.weightTextView.text = String.format("%.2f KG", fundOpportunity?.average_weight)
    binding.sourceTextView.text = fundOpportunity?.source
    binding.locationTextView.text = fundOpportunity?.location?.name
    binding.timelineTextView.text = fundOpportunity?.growth_timeline
    if(fundOpportunity?.details?.length?.equals("") != true){
      binding.fundDetailsTextView.visibility = View.VISIBLE
      binding.fundDetailsTextView.text = fundOpportunity?.details
    }
    else{
      binding.fundDetailsTextView.visibility = View.GONE
    }

    if(!isNetworkAvailable(requireContext())){
      showMsg(requireContext(),"No Internet connection")
    }
    binding.durationValue.text = fundOpportunity?.duration.toString() + " " + getString(R.string.months)
    binding.netProfitValue.visibility = View.GONE
    binding.netProfit.visibility = View.GONE
    binding.returnVal.visibility = View.GONE
    binding.returnValValue.visibility = View.GONE
    if(getValueNetProfit() != 0 ){
      binding.netProfitValue.text = getValueNetProfit().toString() + " "+"BDT"
      binding.netProfitValue.visibility = View.VISIBLE
      binding.netProfit.visibility = View.VISIBLE
    }

    if(fundOpportunity?.profit_percentage != null && fundOpportunity?.shariah_profit_from.isNullOrEmpty() || fundOpportunity?.shariah_profit_to.isNullOrEmpty()){
      binding.returnValValue.text = fundOpportunity?.profit_percentage.toString() + " " + "%"
      binding.returnVal.visibility = View.VISIBLE
      binding.returnValValue.visibility = View.VISIBLE
    }
    else{
      binding.returnValValue.text = fundOpportunity?.shariah_profit_from + "%" +" to " + fundOpportunity?.shariah_profit_to + "%"
      binding.returnVal.visibility = View.VISIBLE
      binding.returnValValue.visibility = View.VISIBLE
    }

    return binding.root
  }
  override fun onAttach(activity: Context) {
    super.onAttach(activity)
    //this.fundAct = activity
    fundOpportunity  = (activity as FundDetailsActivity).getObject()
  }
  private fun getValueNetProfit(): Int {
    return if (fundOpportunity?.amount != null && fundOpportunity?.profit_percentage != null) {
      (fundOpportunity?.amount!!.toInt() * fundOpportunity?.profit_percentage!!.toInt()) / 100
    } else {
      0
    }
  }

  override fun onDestroyView() {
    super.onDestroyView()
  }
  companion object {
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment DescriptionOfItemFragment.
     */
    // TODO: Rename and change types and number of parameters
    @JvmStatic
    fun newInstance(param1: String, param2: String) =
      DescriptionOfItemFragment().apply {
        arguments = Bundle().apply {
          putString(ARG_PARAM1, param1)
          putString(ARG_PARAM2, param2)
        }
      }
  }
}