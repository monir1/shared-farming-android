package com.pranisheba.sharedfarming.ui.fragment
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.pranisheba.sharedfarming.databinding.FragmentHomeBinding
import com.pranisheba.sharedfarming.model.FundOpportunity
import com.pranisheba.sharedfarming.ui.adapter.FundAdapter
import com.pranisheba.sharedfarming.ui.viewmodel.HomeViewModel
import com.pranisheba.sharedfarming.ui.activity.FundDetailsActivity
import com.pranisheba.sharedfarming.ui.activity.NotificationsItemsActivity
import com.pranisheba.sharedfarming.ui.activity.AllFarmsActivity2
import com.pranisheba.sharedfarming.util.*
import android.view.animation.AnimationUtils
import com.pranisheba.sharedfarming.R
import android.widget.LinearLayout
import com.denzcoskun.imageslider.constants.ScaleTypes
import com.denzcoskun.imageslider.interfaces.ItemClickListener
import com.denzcoskun.imageslider.models.SlideModel
import com.pranisheba.sharedfarming.model.BannerWhatsNew


class HomeFragment : Fragment() {
  private lateinit var homeViewModel: HomeViewModel
  private var _binding: FragmentHomeBinding? = null
  var layout: LinearLayout? = null
  private val binding get() = _binding!!
  private var fundOpportunities = ArrayList<FundOpportunity>()
  private var fundOpportunitiesGeneral = ArrayList<FundOpportunity>()
  private var fundOpportunitiesShariah = ArrayList<FundOpportunity>()
  private var banner = ArrayList<SlideModel>()
  private var serverBannerObject = ArrayList<BannerWhatsNew>()
  override fun onCreateView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View {
    homeViewModel =
      ViewModelProvider(this)[HomeViewModel::class.java]
    _binding = FragmentHomeBinding.inflate(inflater, container, false)
    val root: View = binding.root


    homeViewModel.opportunitiesBanner.observe(requireActivity(), {
      serverBannerObject.clear()
      if (it != null) {
        serverBannerObject = it as ArrayList<BannerWhatsNew>
        if(serverBannerObject.size > 0){
          banner.clear()
          binding.homeRecycleImageBanner.setImageList(banner, ScaleTypes.FIT)
          for (i in 0 until serverBannerObject.size) {
            banner.add(SlideModel(serverBannerObject[i].image))
          }
          binding.homeRecycleImageBanner.setImageList(banner, ScaleTypes.FIT)
          binding.homeRecycleImageBanner.startSliding(3000)
          callListener()
        }
      }
      else{
        banner.add(SlideModel(R.drawable.im3))
        banner.add(SlideModel(R.drawable.im3))
        binding.homeRecycleImageBanner.setImageList(banner, ScaleTypes.FIT)
        binding.homeRecycleImageBanner.startSliding(3000)
         callListener()
      }
    })




    binding.swipe.setOnRefreshListener {
      binding.swipe.isRefreshing = false
      homeViewModel.getFundOpportunities()
      homeViewModel.getBannerWhatsNew()
    }
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
      binding.scrollView.setOnScrollChangeListener { _: View, _: Int, scrollY: Int, _: Int, _: Int ->
        binding.swipe.isEnabled = scrollY == 0
      }
    }
    homeViewModel.fundOpportunities.observe(viewLifecycleOwner, {
      //setting recycler to horizontal scroll
      binding.recyclerView.layoutManager =
        LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)

      binding.recyclerViewShariah.layoutManager =
        LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
      binding.recyclerViewRegular.layoutManager =
        LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)

      it.let {
        fundOpportunities.clear()
        for (i in it.indices) {
          if (it[i].is_active == true) {
            fundOpportunities.add(it[i])
          }
        }
        //setting adapter to recycler
        binding.recyclerView.adapter = FundAdapter(fundOpportunities, ::onFundItemClickAll)
        if (fundOpportunities.size != 0) {
          val length = fundOpportunities.size
          if (length > 0) {
            fundOpportunitiesGeneral.clear()
            fundOpportunitiesShariah.clear()
            for (i in 0 until length) {
              if (fundOpportunities[i].category.equals("general")) {
                val value = fundOpportunities[i]
                fundOpportunitiesGeneral.add(value)
              } else if (fundOpportunities[i].category.equals("shariah")) {
                val value = fundOpportunities[i]
                fundOpportunitiesShariah.add(value)
              }
            }
            binding.recyclerViewRegular.adapter =
              FundAdapter(fundOpportunitiesGeneral, ::onFundItemClickGeneral)
            binding.recyclerViewShariah.adapter =
              FundAdapter(fundOpportunitiesShariah, ::onFundItemClickShariah)

            if (fundOpportunitiesGeneral.size > 1) {
              binding.constraintLayout5.visibility = View.VISIBLE
            }
            if (fundOpportunitiesShariah.size > 1) {
              binding.constraintLayout3.visibility = View.VISIBLE
            }
          }
        }
      }
    })

    homeViewModel.progress.observe(viewLifecycleOwner, {
      if (it) {
        binding.animationView.visibility = View.VISIBLE
      } else {
        binding.animationView.visibility = View.GONE
      }
    })

    homeViewModel.fundOpportunity.observe(requireActivity(), {
      if (it != null) {
        val intent = Intent(context, FundDetailsActivity::class.java)
        intent.putExtra(FUND_OPPORTUNITY, it)
        startActivity(intent)
      } else {
        showSnackBar(binding.blogTextView, "Nothing to view !!")
      }
    })

    binding.notificationItems.setOnClickListener {
      val intent = Intent(context, NotificationsItemsActivity::class.java)
      startActivity(intent)

    }

      homeViewModel.getBannerWhatsNew()

    homeViewModel.getFundOpportunities()
    binding.fundOpportunitiesAllTextView.setOnClickListener {
      seeAll()
    }
    binding.regularFundOpportunitiesAllTextView.setOnClickListener {
      regularSeeAll()
    }
    binding.shariahFundOpportunitiesAllTextView.setOnClickListener {
      shariahSeeAll()
    }

    val animation = AnimationUtils.loadAnimation(
      context,
      R.anim.sample_animation
    )
    binding.totalInvestmentValue.startAnimation(animation)
    binding.farmProduceValue.startAnimation(animation)
    binding.returnForFarmFundersValue.startAnimation(animation)
    binding.registerFarmersValue.startAnimation(animation)

    return root
  }

  private fun regularSeeAll() {
    if (fundOpportunitiesGeneral.size < 1) {
      showMsg(requireContext(), "Please Wait")
    } else {
      val intent = Intent(context, AllFarmsActivity2::class.java)
      intent.putExtra(ALL_FUND_OPPORTUNITY, fundOpportunities)
      intent.putExtra(REGULAR_OPPORTUNITY, fundOpportunitiesGeneral)
      intent.putExtra(SHARIAH_OPPORTUNITY, fundOpportunitiesShariah)
      intent.putExtra(WHAT_TO_LOAD_FUND, 2)
      startActivity(intent)
    }
  }

  private fun shariahSeeAll() {
    if (fundOpportunitiesShariah.size < 1) {
      showMsg(requireContext(), "Please Wait")
    } else {
      val intent = Intent(context, AllFarmsActivity2::class.java)
      intent.putExtra(ALL_FUND_OPPORTUNITY, fundOpportunities)
      intent.putExtra(REGULAR_OPPORTUNITY, fundOpportunitiesGeneral)
      intent.putExtra(SHARIAH_OPPORTUNITY, fundOpportunitiesShariah)
      intent.putExtra(WHAT_TO_LOAD_FUND, 3)
      startActivity(intent)
    }
  }

  private fun onFundItemClickGeneral(position: Int) {
    val intent = Intent(context, FundDetailsActivity::class.java)
    intent.putExtra(FUND_OPPORTUNITY, fundOpportunitiesGeneral[position])
    startActivity(intent)
  }

  private fun onFundItemClickShariah(position: Int) {
    val intent = Intent(context, FundDetailsActivity::class.java)
    intent.putExtra(FUND_OPPORTUNITY, fundOpportunitiesShariah[position])
    startActivity(intent)
  }

  private fun onFundItemClickAll(position: Int) {
    val intent = Intent(context, FundDetailsActivity::class.java)
    intent.putExtra(FUND_OPPORTUNITY, fundOpportunities[position])
    startActivity(intent)
  }

  private fun seeAll() {
    if (fundOpportunities.size < 1) {
      showMsg(requireContext(), "Please Wait")
    } else {
      val intent = Intent(context, AllFarmsActivity2::class.java)
      intent.putExtra(ALL_FUND_OPPORTUNITY, fundOpportunities)
      intent.putExtra(REGULAR_OPPORTUNITY, fundOpportunitiesGeneral)
      intent.putExtra(SHARIAH_OPPORTUNITY, fundOpportunitiesShariah)
      intent.putExtra(WHAT_TO_LOAD_FUND, 1)
      startActivity(intent)
    }
  }

  override fun onDestroyView() {
    super.onDestroyView()
    //_binding = null
  }

  companion object {
    const val TAG = "HomeFragment"
  }

  private fun callListener() {
    binding.homeRecycleImageBanner.setItemClickListener(object : ItemClickListener {
      override fun onItemSelected(position: Int) {
        if (serverBannerObject.size > 0) {
          val id = serverBannerObject[position].product
          if (id != null) {
            showMsg(requireContext(),"Please wait..")
            homeViewModel.getFundOpportunity(id)
          } else {
            showSnackBar(binding.blogTextView, "Nothing to view !!")
          }
        }
        else{
          showSnackBar(binding.blogTextView, "Nothing to view !!")
        }
      }
    })
  }


}