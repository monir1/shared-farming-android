package com.pranisheba.sharedfarming.preference

import android.content.Context
import android.content.SharedPreferences
import com.pranisheba.sharedfarming.util.*


const val PREFERENCE_TITLE = "SharedFarmingPreference"

class SharedFarmingPreference(context: Context) {
  private val preferences: SharedPreferences =
    context.getSharedPreferences(PREFERENCE_TITLE, Context.MODE_PRIVATE)
  private val editor: SharedPreferences.Editor = preferences.edit()

  fun putAuthToken(token: String?) {
    editor.putString(AUTH_TOKEN, token)
    editor.apply()
  }

  fun getAuthToken(): String? {
    return preferences.getString(AUTH_TOKEN, "")
  }


  fun putAuthPhone(mobile: String?) {
    editor.putString(PHONE, mobile)
    editor.apply()
  }

  fun getAuthPhone(): String? {
    return preferences.getString(PHONE, "")
  }

  fun putAuthMail(mail: String?) {
    editor.putString(EMAIL, mail)
    editor.apply()
  }

  fun getAuthMail(): String? {
    return preferences.getString(EMAIL, "")
  }


  fun putExpTokenTime(expIn : String) {
    editor.putString(EXP_TIME, expIn)
    editor.apply()
  }

  fun getExpTokenTime(): String? {
    return preferences.getString(EXP_TIME, "")
  }



}
