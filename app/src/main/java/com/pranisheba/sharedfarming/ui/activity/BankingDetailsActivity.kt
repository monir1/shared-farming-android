package com.pranisheba.sharedfarming.ui.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.pranisheba.sharedfarming.R
import com.pranisheba.sharedfarming.databinding.ActivityBankingDetailsBinding
import com.pranisheba.sharedfarming.model.BankingModel
import com.pranisheba.sharedfarming.model.ProfileModel
import com.pranisheba.sharedfarming.model.ProfilePersonalItem
import com.pranisheba.sharedfarming.preference.SharedFarmingPreference
import com.pranisheba.sharedfarming.ui.adapter.PersonalDetailsItemAdapter
import com.pranisheba.sharedfarming.ui.viewmodel.BankingViewModel
import com.pranisheba.sharedfarming.util.*
import com.squareup.picasso.Picasso

class BankingDetailsActivity : AppCompatActivity() {
  private lateinit var binding: ActivityBankingDetailsBinding
  private lateinit var personalDetails: ProfileModel
  private lateinit var viewModel : BankingViewModel
  private  var bankList = ArrayList<BankingModel> ()
  private lateinit var banking : BankingModel
  private var bankItem = ArrayList<ProfilePersonalItem>()


  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    binding = ActivityBankingDetailsBinding.inflate(layoutInflater)
    setContentView(binding.root)
    viewModel = ViewModelProvider(this)[BankingViewModel::class.java]
    supportActionBar?.setDisplayHomeAsUpEnabled(true)
    supportActionBar?.setDisplayShowHomeEnabled(true)
    supportActionBar?.setTitle(R.string.banking_details)
    binding.edit.visibility = View.GONE
    binding.bankInfo.visibility = View.GONE
    binding.bankingRecycle.visibility = View.GONE
    personalDetails = intent.getParcelableExtra(PROFILE_INFO)!!
    getCommonProfileData()
    binding.bankingRecycle.layoutManager =
      LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
    getBankDetails()
    viewModel.progress.observe(this, {
      if (it) {
        binding.animationView.visibility = View.VISIBLE
      } else {
        binding.animationView.visibility = View.GONE
      }
    })

    viewModel.tokenRemove.observe(this, {
      if(it){
        showMsg(this, "Log In to view Info")
        SharedFarmingPreference(this).putAuthToken(null)
        startActivity(Intent(this,LoginActivity::class.java))
        finish()
      }
    })
  }

  private fun getBankDetails() {
    val token  = "token "+ SharedFarmingPreference(this).getAuthToken().toString()
    viewModel.getBanking(token)
    viewModel.banking.observe(this,  {
      if(it != null){
        bankList = it as ArrayList<BankingModel>
        bankItem.clear()

        if(bankList.size > 0){
          banking = bankList[0]
       //   binding.edit.text = getString(R.string.updateNom)
          binding.edit.visibility = View.VISIBLE
          binding.edit.setOnClickListener {
            val intent = Intent(this, UpdateOrCreateBankingActivity::class.java)
            intent.putExtra(UPDATE_CREATE_BANK_INFO, "update")
            intent.putExtra(UPDATE_CREATE_BANK, banking)
            intent.putExtra(PROFILE_INFO,personalDetails)
            startActivity(intent)
          }
          binding.bankingRecycle.visibility = View.VISIBLE
          bankItem.add(ProfilePersonalItem("Name",banking.name))
          bankItem.add(ProfilePersonalItem("Branch Name",banking.branch_name))
          bankItem.add(ProfilePersonalItem("Account No",banking.account_no))
          bankItem.add(ProfilePersonalItem("Account Name",banking.Account_name))
          binding.bankingRecycle.adapter = PersonalDetailsItemAdapter(bankItem)
        }
        else{
          binding.bankInfo.visibility = View.VISIBLE
        //  binding.edit.text = getString(R.string.createNom)
          binding.edit.visibility = View.VISIBLE
          binding.edit.setOnClickListener {
            val intent = Intent(this, UpdateOrCreateBankingActivity::class.java)
            intent.putExtra(UPDATE_CREATE_BANK_INFO, "create")
            intent.putExtra(PROFILE_INFO,personalDetails)
            startActivity(intent)
          }
        }
      }
    })
  }

  override fun onOptionsItemSelected(item: MenuItem): Boolean {
    // handle arrow click here
    if (item.itemId == android.R.id.home) {
      onBackPressed()
    }
    return super.onOptionsItemSelected(item)
  }
  private fun getCommonProfileData() {
    binding.name.text = personalDetails.name
    binding.mail.text = SharedFarmingPreference(this).getAuthMail()
    binding.phone.text = SharedFarmingPreference(this).getAuthPhone()
    val url = personalDetails.profile_img.toString()
    //Loading Image into view
    Picasso.get()
      .load(url)
      .placeholder(R.drawable.ic_baseline_person_24)
      .error(R.drawable.ic_baseline_person_24)
      .into(binding.profileImage)
  }

  companion object {
    const val TAG = "Banking Info Details Activity"
  }
}