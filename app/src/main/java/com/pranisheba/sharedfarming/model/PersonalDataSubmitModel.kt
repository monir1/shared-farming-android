package com.pranisheba.sharedfarming.model
import android.os.Parcelable
import kotlinx.parcelize.Parcelize

  @Parcelize
  data class PersonalDataSubmitModel(
    var name: String?,
    var nid: String?,
    var dob: String?,
    var address1: String?,
    var address2: String?,
    var zip_code: String?,
    var company_name: String?,
    var occupation: String?,
    var nationality: String?,
    var city: String?,
    var country: String?
  ) : Parcelable
