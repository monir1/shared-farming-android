package com.pranisheba.sharedfarming.ui.activity
import android.annotation.SuppressLint
import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.InputType
import android.util.Log
import androidx.lifecycle.ViewModelProvider
import com.pranisheba.sharedfarming.R
import com.pranisheba.sharedfarming.databinding.ActivityProfileEditBinding
import com.pranisheba.sharedfarming.model.PersonalDataSubmitModel
import com.pranisheba.sharedfarming.model.ProfileModel
import com.pranisheba.sharedfarming.preference.SharedFarmingPreference
import com.pranisheba.sharedfarming.ui.viewmodel.ProfileViewModel
import com.pranisheba.sharedfarming.util.PROFILE_INFO
import java.util.*
import com.pranisheba.sharedfarming.util.showMsg
import okhttp3.MultipartBody
import java.io.*
import android.content.Context
import android.os.Build
import androidx.annotation.RequiresApi
import android.content.ContextWrapper
import android.graphics.BitmapFactory
import android.view.*
import android.widget.LinearLayout
import com.canhub.cropper.CropImageContract
import com.canhub.cropper.CropImageView
import com.canhub.cropper.options
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.pranisheba.sharedfarming.util.PROFILE_CONTACT_BASE
import com.squareup.picasso.Picasso
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.asRequestBody
import java.lang.Exception



class ProfileEditActivity : AppCompatActivity() {
  private lateinit var personalDetails: ProfileModel
  private lateinit var binding: ActivityProfileEditBinding
  private lateinit var viewModel: ProfileViewModel
  private lateinit var customDialog : BottomSheetDialog
  private var multipartImage: MultipartBody.Part? = null
  private var fileOfMultiPart: File? = null
  private lateinit var contactOrNot: String

  @RequiresApi(Build.VERSION_CODES.Q)
  @SuppressLint("SetTextI18n")
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    binding = ActivityProfileEditBinding.inflate(layoutInflater)
    setContentView(binding.root)

    supportActionBar?.setDisplayHomeAsUpEnabled(true)
    supportActionBar?.setDisplayShowHomeEnabled(true)
    supportActionBar?.setTitle(R.string.edit_personal_details)
    personalDetails = intent.getParcelableExtra(PROFILE_INFO)!!
    contactOrNot = intent.getStringExtra(PROFILE_CONTACT_BASE)!!
    customDialog = BottomSheetDialog(this)
    viewModel = ViewModelProvider(this)[ProfileViewModel::class.java]
    Log.d("Get Personal Data2 : ", personalDetails.toString())
    getProfileEditableData()
    binding.submitData.setOnClickListener {
      dataSubmitToEdit()
    }
    viewModel.progress.observe(this, {
      if (it) {
        binding.animationView.visibility = View.VISIBLE
      } else {
        binding.animationView.visibility = View.GONE
      }
    })
    binding.itemDescriptionDateOfBirth.inputType = InputType.TYPE_NULL
    binding.itemDescriptionDateOfBirth.keyListener = null
    viewModel.tokenRemove.observe(this, {
      if (it) {
        showMsg(this, "Login to view your Info")
        SharedFarmingPreference(this@ProfileEditActivity).putAuthToken(null)
        startActivity(Intent(this@ProfileEditActivity, MainActivity::class.java))
        finish()
      }
    })

    val url = personalDetails.profile_img.toString()
    //Loading Image into view
    Picasso.get()
      .load(url)
      .placeholder(R.drawable.ic_baseline_person_24)
      .error(R.drawable.ic_baseline_person_24)
      .into(binding.profileImage)

    binding.itemDescriptionDateOfBirth.setOnClickListener {
      val c = Calendar.getInstance()
      val year = c.get(Calendar.YEAR)
      val month = c.get(Calendar.MONTH)
      val day = c.get(Calendar.DAY_OF_MONTH)
      val dpd =
        DatePickerDialog(this, R.style.my_dialog_theme, { _, yearExact, monthOfYear, dayOfMonth ->
          binding.itemDescriptionDateOfBirth.setText("$yearExact-${monthOfYear + 1}-$dayOfMonth")
        }, year, month, day)

      dpd.show()
    }

    binding.takePic.setOnClickListener {
      openDialogToGetImage(customDialog)
    }


    viewModel.userProfileEditImage.observe(this, {
      showMsg(this, "Uploaded Image successfully")
    })
    viewModel.userProfileEdit.observe(this, {
      if (it != null) {
        personalDetails = it
        getProfileEditableData()

          showMsg(this, "Updated Info successfully")
          val intent = Intent(this, PersonalInfoDetailsActivity::class.java)
          intent.putExtra(PROFILE_CONTACT_BASE, contactOrNot)
          startActivity(intent)

      }
    })
  }




  @SuppressLint("QueryPermissionsNeeded", "UnsupportedChromeOsCameraSystemFeature")
  private fun openDialogToGetImage(customDialog: BottomSheetDialog) {
    customDialog.setContentView(R.layout.dialog_take_photo)
    val cameraBtn: LinearLayout? = customDialog.findViewById(R.id.camera)
    val folderBtn: LinearLayout? = customDialog.findViewById(R.id.folder)
    cameraBtn?.setOnClickListener {
    clickAnImage()
    }
    folderBtn?.setOnClickListener {
     gallery()
    }
    customDialog.show()
  }

  private fun gallery(){
    cropImage.launch(
      options {
        setImageSource(true,false)
        setCropShape(CropImageView.CropShape.OVAL)
        setAspectRatio(1,1)
        setActivityTitle("Crop Image")
      }
    )
  }
  private fun clickAnImage(){
    cropImage.launch(
      options {
        setImageSource(false,true)
        setCropShape(CropImageView.CropShape.OVAL)
        setAspectRatio(1,1)
        setActivityTitle("Crop Image")
      }
    )
  }

  private fun dataSubmitToEdit() {
    val name = binding.itemDescriptionName.editText?.text.toString().trim()
    val address1 = binding.itemDescriptionAddress1.editText?.text.toString().trim()
    val address2 = binding.itemDescriptionAddress2.editText?.text.toString().trim()
    val dob = binding.itemDescriptionDateOfBirth.text.toString()
    val zip = binding.itemDescriptionZip.editText?.text.toString().trim()
    val country = binding.itemDescriptionCountry.editText?.text.toString().trim()
    val city = binding.itemDescriptionCity.editText?.text.toString().trim()
    val nid = binding.itemDescriptionNid.editText?.text.toString().trim()
    val cName = binding.itemDescriptionCompanyName.editText?.text.toString().trim()
    val occupation = binding.itemDescriptionOccupation.editText?.text.toString().trim()
    val nationality = binding.itemDescriptionNationality.editText?.text.toString().trim()
    if (name.isEmpty()) {
      binding.itemDescriptionName.error = getString(R.string.must)
      binding.itemDescriptionName.requestFocus()
      return
    }
    if (address1.isEmpty()) {
      binding.itemDescriptionAddress1.error = getString(R.string.must)
      binding.itemDescriptionAddress1.requestFocus()
      return
    }
    if (nid.isEmpty()) {
      binding.itemDescriptionNid.error = getString(R.string.must)
      binding.itemDescriptionNid.requestFocus()
      return
    }
    if (dob.isEmpty()) {
      binding.itemDescriptionDateOfBirth.error = getString(R.string.must)
      binding.itemDescriptionDateOfBirth.requestFocus()
      return
    }
    if (city.isEmpty()) {
      binding.itemDescriptionCity.error = getString(R.string.must)
      binding.itemDescriptionCity.requestFocus()
      return
    }
    if (country.isEmpty()) {
      binding.itemDescriptionCountry.error = getString(R.string.must)
      binding.itemDescriptionCountry.requestFocus()
      return
    }
    if (address2.isEmpty()) {
      binding.itemDescriptionAddress2.error = getString(R.string.must)
      binding.itemDescriptionAddress2.requestFocus()
      return
    }
    if (zip.isEmpty()) {
      binding.itemDescriptionZip.error = getString(R.string.must)
      binding.itemDescriptionZip.requestFocus()
      return
    }

    viewModel.submitProfileEdit(
      "token " + SharedFarmingPreference(this@ProfileEditActivity).getAuthToken().toString(),
      personalDetails.id!!,
      PersonalDataSubmitModel(
        name,
        nid,
        dob,
        address1,
        address2,
        zip,
        cName,
        occupation,
        nationality,
        city,
        country
      )
    )


  }

  private fun getProfileEditableData() {
    binding.itemDescriptionName.editText?.setText(personalDetails.name)
    binding.itemDescriptionAddress1.editText?.setText(personalDetails.address1)
    binding.itemDescriptionAddress2.editText?.setText(personalDetails.address2)
    binding.itemDescriptionDateOfBirth.setText(personalDetails.dob)
    binding.itemDescriptionZip.editText?.setText(personalDetails.zip_code)
    binding.itemDescriptionCity.editText?.setText(personalDetails.city)
    binding.itemDescriptionCountry.editText?.setText(personalDetails.country)
    binding.itemDescriptionNid.editText?.setText(personalDetails.nid)
    binding.itemDescriptionCompanyName.editText?.setText(personalDetails.company_name)
    binding.itemDescriptionOccupation.editText?.setText(personalDetails.occupation)
    binding.itemDescriptionNationality.editText?.setText(personalDetails.nationality)
  }

  override fun onOptionsItemSelected(item: MenuItem): Boolean {
    // handle arrow click here
    if (item.itemId == android.R.id.home) {
      onBackPressed()
    }
    return super.onOptionsItemSelected(item)
  }

  companion object {
    const val TAG = "Profile Edit Activity"
  }

  // Method to save an bitmap to a file
  private fun bitmapToFile(bitmap: Bitmap): Uri {
    // Get the context wrapper
    val wrapper = ContextWrapper(applicationContext)

    // Initialize a new file instance to save bitmap object
    var file = wrapper.getDir("Images", Context.MODE_PRIVATE)
    file = File(file, "${UUID.randomUUID()}.jpg")

    try {
      // Compress the bitmap and save in jpg format
      val stream: OutputStream = FileOutputStream(file)
      bitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream)
      stream.flush()
      stream.close()
    } catch (e: IOException) {
      e.printStackTrace()
    }

    fileOfMultiPart = saveBitmapToFile(file)
    Log.d("file size 2 :: " , fileOfMultiPart?.length().toString())

    // Return the saved bitmap uri
    return Uri.parse(file.absolutePath)
  }

  @Throws(FileNotFoundException::class, IOException::class)

  fun getBitmapFormUri(ac: Activity, uri: Uri?): Bitmap? {
    var input = ac.contentResolver.openInputStream(uri!!)
    val onlyBoundsOptions = BitmapFactory.Options()
    onlyBoundsOptions.inJustDecodeBounds = true
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
      // Call some material design APIs here
    } else {
      onlyBoundsOptions.inDither = true //optional
    }
    onlyBoundsOptions.inPreferredConfig = Bitmap.Config.ARGB_8888 //optional
    BitmapFactory.decodeStream(input, null, onlyBoundsOptions)
    input!!.close()
    val originalWidth = onlyBoundsOptions.outWidth
    val originalHeight = onlyBoundsOptions.outHeight
    if (originalWidth == -1 || originalHeight == -1) return null
    //Image resolution is based on 480x800
    val hh = 800f //The height is set as 800f here
    val ww = 480f //Set the width here to 480f
    //Zoom ratio. Because it is a fixed scale, only one data of height or width is used for calculation
    var be = 1 //be=1 means no scaling
    if (originalWidth > originalHeight && originalWidth > ww) { //If the width is large, scale according to the fixed size of the width
      be = (originalWidth / ww).toInt()
    } else if (originalWidth < originalHeight && originalHeight > hh) { //If the height is high, scale according to the fixed size of the width
      be = (originalHeight / hh).toInt()
    }
    if (be <= 0) be = 1
    //Proportional compression
    val bitmapOptions = BitmapFactory.Options()
    bitmapOptions.inSampleSize = be //Set scaling

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
      // Call some material design APIs here
    } else {
      bitmapOptions.inDither = true //optional
    }
    bitmapOptions.inPreferredConfig = Bitmap.Config.ARGB_8888 //optional
    input = ac.contentResolver.openInputStream(uri)
    val bitmap = BitmapFactory.decodeStream(input, null, bitmapOptions)
    input!!.close()
    return bitmap
  }

  private val cropImage = registerForActivityResult(CropImageContract()) { result ->
    if (result.isSuccessful) {
      customDialog.dismiss()
      // use the returned uri
      val uriContent = result.uriContent
      binding.profileImage.setImageURI(uriContent)
     // val uriFilePath = result.getUriFilePath(this) // optional usage
      if (uriContent != null) {
        getBitmapFormUri(this,uriContent)?.let { bitmapToFile(it) }
      }

      if (fileOfMultiPart != null) {
        val filePart = fileOfMultiPart!!.asRequestBody("image/*".toMediaTypeOrNull())
        multipartImage =
          MultipartBody.Part.createFormData("profile_img", fileOfMultiPart!!.name, filePart)
        if (multipartImage != null) {
          viewModel.uploadImage(
            "token " + SharedFarmingPreference(this@ProfileEditActivity).getAuthToken().toString(),
            personalDetails.id!!, multipartImage!!
          )
        }
      }

    } else {
      // an error occurred
      val exception = result.error
      Log.d("exception",exception.toString())
    }
  }

  fun saveBitmapToFile(file: File): File? {
    return try {

      // BitmapFactory options to downsize the image
      val o = BitmapFactory.Options()
      o.inJustDecodeBounds = true
      o.inSampleSize = 6
      // factor of downsizing the image
      var inputStream = FileInputStream(file)
      //Bitmap selectedBitmap = null;
      BitmapFactory.decodeStream(inputStream, null, o)
      inputStream.close()

      // The new size we want to scale to
      val REQUIRED_SIZE = 50

      // Find the correct scale value. It should be the power of 2.
      var scale = 1
      while (o.outWidth / scale / 2 >= REQUIRED_SIZE &&
        o.outHeight / scale / 2 >= REQUIRED_SIZE
      ) {
        scale *= 2
      }
      val o2 = BitmapFactory.Options()
      o2.inSampleSize = scale
      inputStream = FileInputStream(file)
      val selectedBitmap = BitmapFactory.decodeStream(inputStream, null, o2)
      inputStream.close()

      // here i override the original image file
      file.createNewFile()
      val outputStream = FileOutputStream(file)
      selectedBitmap!!.compress(Bitmap.CompressFormat.JPEG, 100, outputStream)
      file
    } catch (e: Exception) {
      null
    }
  }

}