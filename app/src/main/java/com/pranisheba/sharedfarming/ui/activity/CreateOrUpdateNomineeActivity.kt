package com.pranisheba.sharedfarming.ui.activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.lifecycle.ViewModelProvider
import com.pranisheba.sharedfarming.R
import com.pranisheba.sharedfarming.databinding.ActivityCreateOrUpdateNomineeBinding
import com.pranisheba.sharedfarming.model.NomineeModel
import com.pranisheba.sharedfarming.model.ProfileModel
import com.pranisheba.sharedfarming.model.SubmitNomineeInfoModel
import com.pranisheba.sharedfarming.preference.SharedFarmingPreference
import com.pranisheba.sharedfarming.ui.viewmodel.NomineeViewModel
import com.pranisheba.sharedfarming.util.PROFILE_INFO
import com.pranisheba.sharedfarming.util.UPDATE_CREATE_NOMINEE
import com.pranisheba.sharedfarming.util.UPDATE_CREATE_NOMINEE_INFO
import com.pranisheba.sharedfarming.util.showMsg

class CreateOrUpdateNomineeActivity : AppCompatActivity() {
  private lateinit var binding: ActivityCreateOrUpdateNomineeBinding
  private lateinit var viewModel : NomineeViewModel
  private lateinit var nomineeCreate : String
  private lateinit var nominee : NomineeModel
  private lateinit var personalDetails: ProfileModel
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    binding = ActivityCreateOrUpdateNomineeBinding.inflate(layoutInflater)
    setContentView(binding.root)
    viewModel = ViewModelProvider(this)[NomineeViewModel::class.java]
    supportActionBar?.setDisplayHomeAsUpEnabled(true)
    supportActionBar?.setDisplayShowHomeEnabled(true)
    personalDetails = intent.getParcelableExtra(PROFILE_INFO)!!
    nomineeCreate = intent.getStringExtra(UPDATE_CREATE_NOMINEE_INFO)!!

    if(nomineeCreate == "create"){
      supportActionBar?.setTitle(R.string.createNominee)
      createNominee()
    }
    else{
      supportActionBar?.setTitle(R.string.updateNominee)
      nominee = intent.getParcelableExtra(UPDATE_CREATE_NOMINEE)!!
      updateNominee()
    }

    viewModel.postNominee.observe(this,  {
      if(it != null){
        showMsg(this,"Info Updated Successfully")
        val intent = Intent(this,NomineeDetailsActivity::class.java)
        intent.putExtra(PROFILE_INFO,personalDetails)
        startActivity(intent)
        finish()
      }
    })
    viewModel.updateNominee.observe(this,  {
      if(it != null){
        showMsg(this,"Info Updated Successfully")
        val intent = Intent(this,NomineeDetailsActivity::class.java)
        intent.putExtra(PROFILE_INFO,personalDetails)
        startActivity(intent)
        finish()
      }
    })
    viewModel.progress.observe(this, {
      if (it) {
        binding.animationView.visibility = View.VISIBLE
      } else {
        binding.animationView.visibility = View.GONE
      }
    })

    viewModel.tokenRemove.observe(this, {
      if(it){
        showMsg(this, "Log In to view Info")
        SharedFarmingPreference(this).putAuthToken(null)
        startActivity(Intent(this,LoginActivity::class.java))
        finish()
      }
    })
  }

  private fun updateNominee() {
    binding.itemDescriptionName.editText?.setText(nominee.name)
    binding.itemDescriptionContactNo.editText?.setText(nominee.contact_no)
    binding.itemDescriptionNid.editText?.setText(nominee.nid)
    binding.itemDescriptionRelation.editText?.setText(nominee.relationship)
    binding.submitData.setOnClickListener {
      updateNomineeInfo()
    }
  }

  private fun updateNomineeInfo() {
    val token = "token " + SharedFarmingPreference(this).getAuthToken().toString()
    val name = binding.itemDescriptionName.editText?.text.toString()
    val contactNo = binding.itemDescriptionContactNo.editText?.text.toString()
    val nid = binding.itemDescriptionNid.editText?.text.toString()
    val relation = binding.itemDescriptionRelation.editText?.text.toString()

    if(name.isEmpty()){
      binding.itemDescriptionName.error = getString(R.string.thisIsMust)
      binding.itemDescriptionName.requestFocus()
      return
    }
    if(contactNo.isEmpty()){
      binding.itemDescriptionContactNo.error = getString(R.string.thisIsMust)
      binding.itemDescriptionContactNo.requestFocus()
      return
    }
    if(nid.isEmpty()){
      binding.itemDescriptionNid.error = getString(R.string.thisIsMust)
      binding.itemDescriptionNid.requestFocus()
      return
    }
    if(relation.isEmpty()){
      binding.itemDescriptionRelation.error = getString(R.string.thisIsMust)
      binding.itemDescriptionRelation.requestFocus()
      return
    }
    if(name.isNotEmpty() && relation.isNotEmpty() && contactNo.isNotEmpty() && nid.isNotEmpty()){
      viewModel.updateNominee(token, SubmitNomineeInfoModel(name,relation,contactNo,nid),nominee.id!!)
    }
  }

  private fun createNominee() {
    binding.submitData.setOnClickListener {
      newNominee()
    }
  }

  private fun newNominee() {
    val token = "token " + SharedFarmingPreference(this).getAuthToken().toString()
    val name = binding.itemDescriptionName.editText?.text.toString()
    val contactNo = binding.itemDescriptionContactNo.editText?.text.toString()
    val nid = binding.itemDescriptionNid.editText?.text.toString()
    val relation = binding.itemDescriptionRelation.editText?.text.toString()

    if(name.isEmpty()){
      binding.itemDescriptionName.error = getString(R.string.thisIsMust)
      binding.itemDescriptionName.requestFocus()
      return
    }
    if(contactNo.isEmpty()){
      binding.itemDescriptionContactNo.error = getString(R.string.thisIsMust)
      binding.itemDescriptionContactNo.requestFocus()
      return
    }
    if(nid.isEmpty()){
      binding.itemDescriptionNid.error = getString(R.string.thisIsMust)
      binding.itemDescriptionNid.requestFocus()
      return
    }
    if(relation.isEmpty()){
      binding.itemDescriptionRelation.error = getString(R.string.thisIsMust)
      binding.itemDescriptionRelation.requestFocus()
      return
    }
    if(name.isNotEmpty() && relation.isNotEmpty() && contactNo.isNotEmpty() && nid.isNotEmpty()){
      viewModel.postNominee(token, SubmitNomineeInfoModel(name,relation,contactNo,nid))
    }

  }


  override fun onOptionsItemSelected(item: MenuItem): Boolean {
    // handle arrow click here
    if (item.itemId == android.R.id.home) {
      onBackPressed()
    }
    return super.onOptionsItemSelected(item)
  }

  companion object {
    const val TAG = "Nominee Create Update Activity"
  }

}