package com.pranisheba.sharedfarming.ui.activity

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Patterns
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.core.util.PatternsCompat.EMAIL_ADDRESS
import androidx.lifecycle.ViewModelProvider
import com.pranisheba.sharedfarming.R
import com.pranisheba.sharedfarming.databinding.ActivityForgetPasswordBinding
import com.pranisheba.sharedfarming.model.SubmitToResetPass
import com.pranisheba.sharedfarming.ui.viewmodel.ForgetPassViewModel
import com.pranisheba.sharedfarming.util.isNetworkAvailable
import com.pranisheba.sharedfarming.util.showMsg
import com.pranisheba.sharedfarming.util.showNoInternetBar
import com.pranisheba.sharedfarming.util.showSnackBar
import java.lang.Exception

class ForgetPasswordActivity : AppCompatActivity() {
  private lateinit var binding: ActivityForgetPasswordBinding
  lateinit var viewModel: ForgetPassViewModel
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    binding = ActivityForgetPasswordBinding.inflate(layoutInflater)
    viewModel = ViewModelProvider(this)[ForgetPassViewModel::class.java]
    setContentView(binding.root)
    binding.passSubmitScrollView.visibility = View.GONE
    binding.sendOtpScrollView.visibility = View.VISIBLE
    supportActionBar?.setDisplayHomeAsUpEnabled(true)
    supportActionBar?.setDisplayShowHomeEnabled(true)
    supportActionBar?.setTitle(R.string.forgetPass)

    if(!isNetworkAvailable(this)){
      showNoInternetBar(binding.layoutAlreadyRemember)
    }
    viewModel.forgetPass.observe(this, {
      if (it != null) {
        if (it.email.isNotEmpty()) {
          showSnackBar(
            binding.etEmailPhone,
            "An OTP has sent to your Email - ${it.email.take(4)}*****"
          )
          binding.passSubmitScrollView.visibility = View.VISIBLE
          binding.sendOtpScrollView.visibility = View.GONE
        } else {
          showSnackBar(binding.etEmailPhone, "An OTP has sent to requested USER NO ")
          binding.passSubmitScrollView.visibility = View.VISIBLE
          binding.sendOtpScrollView.visibility = View.GONE
        }
      }
    })
    binding.btForget.setOnClickListener {
      submitInfoToGetOtp()
    }
    binding.forgetPassResetButton.setOnClickListener {
      submitNewPass()
    }

    binding.layoutAlreadyRemember.setOnClickListener {
      startActivity(Intent(this, LoginActivity::class.java))
    }

    viewModel.progress.observe(this, {
      if (it) {
        binding.animationView.visibility = View.VISIBLE
      } else {
        binding.animationView.visibility = View.GONE
      }
    })
    viewModel.passSubmitReport.observe(this, {
      showMsg(this,it.get("message").toString())
      //showMsg(this,"Updated successfully")
      startActivity(Intent(this, MainActivity::class.java))
      finish()
    })
  }

  private fun submitNewPass() {
    try {
      val imm: InputMethodManager =
        getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
      imm.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
    } catch (e: Exception) {

    }
    val otp = binding.otpLayout.editText?.text.toString().trim()
    val password = binding.passwordLayout.editText?.text.toString().trim()
    val retypePass = binding.retypePasswordLayout.editText?.text.toString().trim()

    if (otp.isEmpty()) {
      binding.otpLayout.error = getString(R.string.otp)
      binding.otpLayout.requestFocus()
      return
    }
    if (password.isEmpty()) {
      binding.passwordLayout.error = getString(R.string.enterPassProperly)
      binding.passwordLayout.requestFocus()
      return
    }


    if (password == retypePass) {
     //  showMsg(this, "Please wait for a moment")

        viewModel.resetPass(SubmitToResetPass(otp, password))

    } else {
      showMsg(this, "Type same password")
      binding.retypePasswordLayout.error = getString(R.string.retype_password_required)
      binding.retypePasswordLayout.requestFocus()
      return
    }
  }

  private fun submitInfoToGetOtp() {
    try {
      val imm: InputMethodManager =
        getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
      imm.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
    } catch (e: Exception) {

    }
    val emailOrPhone = binding.etEmailPhone.text.toString().trim()
    if (emailOrPhone == "") {
      binding.etEmailPhone.error = "Please Enter Email Or Phone"
      binding.etEmailPhone.requestFocus()
      return
    }
    // checking the proper email format or Phone
    if (!isEmailValid(emailOrPhone) && !isValidCellPhone(emailOrPhone)) {
      binding.etEmailPhone.error = "Please Enter Valid Email Or Phone"
      binding.etEmailPhone.requestFocus()
      return
    } else {
       // showMsg(this, "Please wait for a moment")

        viewModel.getOTP(emailOrPhone)

    }
  }

  override fun onOptionsItemSelected(item: MenuItem): Boolean {
    // handle arrow click here
    if (item.itemId == android.R.id.home) {
      onBackPressed()
    }
    return super.onOptionsItemSelected(item)
  }

  private fun isEmailValid(email: String): Boolean {
    return EMAIL_ADDRESS.matcher(email).matches()
  }

  private fun isValidCellPhone(number: String?): Boolean {
    return Patterns.PHONE.matcher(number).matches()
  }
}