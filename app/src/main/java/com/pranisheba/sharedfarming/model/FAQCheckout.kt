package com.pranisheba.sharedfarming.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class FAQCheckout(
 var id : Int?,
 var   question :	String?,
 var   answer :	String?,
 var  category:String
) : Parcelable
