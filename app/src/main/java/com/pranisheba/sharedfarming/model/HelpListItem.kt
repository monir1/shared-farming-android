package com.pranisheba.sharedfarming.model

data class HelpListItem(
  var itemText : String?,
  var itemDescription : String?
)
