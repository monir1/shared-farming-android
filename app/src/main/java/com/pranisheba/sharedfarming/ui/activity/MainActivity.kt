package com.pranisheba.sharedfarming.ui.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.pranisheba.sharedfarming.R
import com.pranisheba.sharedfarming.databinding.ActivityMainBinding
import androidx.appcompat.app.AppCompatDelegate
import androidx.navigation.ui.*
import com.pranisheba.sharedfarming.util.isNetworkAvailable
import com.pranisheba.sharedfarming.util.showNoInternetBar

class MainActivity : AppCompatActivity() {

  private lateinit var binding: ActivityMainBinding
  private lateinit var appBarConfiguration : AppBarConfiguration

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
    binding = ActivityMainBinding.inflate(layoutInflater)
    setContentView(binding.root)
    val navView: BottomNavigationView = binding.navView
    val navController = findNavController(R.id.nav_host_fragment_activity_main)
    appBarConfiguration = AppBarConfiguration(
      setOf(
        R.id.navigation_home, R.id.navigation_my_farms, R.id.navigation_offers, R.id.navigation_more
      )
    )
    setupActionBarWithNavController(navController, appBarConfiguration)
    navView.setupWithNavController(navController)
    this.supportActionBar?.hide()

    if(!isNetworkAvailable(this)){
      showNoInternetBar(binding.container)
    }
  }

  override fun onBackPressed() {
    super.onBackPressed()
    if(findNavController(R.id.nav_host_fragment_activity_main).currentDestination!!.equals(R.id.navigation_home)){
      finishAffinity()
    }
  }



}