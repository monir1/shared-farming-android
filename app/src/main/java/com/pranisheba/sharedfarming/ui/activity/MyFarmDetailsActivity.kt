package com.pranisheba.sharedfarming.ui.activity
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.navigation.findNavController
import com.aceinteract.android.stepper.StepperNavListener
import com.pranisheba.sharedfarming.databinding.ActivityMyFarmDetailsBinding
import com.pranisheba.sharedfarming.model.Invoice
import com.pranisheba.sharedfarming.util.INVOICE
import com.pranisheba.sharedfarming.R
import com.pranisheba.sharedfarming.ui.adapter.SequenceDetectAdapt

class MyFarmDetailsActivity : AppCompatActivity(), StepperNavListener {
  private lateinit var binding: ActivityMyFarmDetailsBinding
  private var currentStep = 0
  private var invoice: Invoice? = null
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    binding = ActivityMyFarmDetailsBinding.inflate(layoutInflater)
    setContentView(binding.root)
    supportActionBar?.setDisplayHomeAsUpEnabled(true)
    supportActionBar?.setDisplayShowHomeEnabled(true)
    invoice = intent.getParcelableExtra(INVOICE)

    supportActionBar?.title = invoice?.product?.name
    binding.stepper.setupWithNavController(findNavController(R.id.frame_stepper))
    binding.stepper.stepperNavListener = this

    val itemsList = ArrayList<SequenceDetectAdapt.FarmDetailItemVertically>()
    val tempItemsList = ArrayList<SequenceDetectAdapt.FarmDetailItemVertically>()
    itemsList.add(SequenceDetectAdapt.FarmDetailItemVertically(true, "Date 4", "work 4", "4"))
    itemsList.add(SequenceDetectAdapt.FarmDetailItemVertically(false, "Date 3", "work 3", "3"))
    itemsList.add(SequenceDetectAdapt.FarmDetailItemVertically(false, "Date 2", "work 2", "2"))
    itemsList.add(SequenceDetectAdapt.FarmDetailItemVertically(false, "Date 1", "work 1", "1"))

    tempItemsList.add(itemsList[0])
    binding.sequence.setAdapter(SequenceDetectAdapt(itemsList))

  }


  override fun onCompleted() {

  }

  override fun onStepChanged(step: Int) {
    currentStep = step
  }

  override fun onOptionsItemSelected(item: MenuItem): Boolean {
    // handle arrow click here
    if (item.itemId == android.R.id.home) {
      finish()
    }
    return super.onOptionsItemSelected(item)
  }

  fun getObject(): Invoice {
    val timeline = 1
    if (currentStep == 0)
      for (i in 0 until timeline - 1) {
        binding.stepper.goToNextStep()
      }
    return invoice!!
  }

}


