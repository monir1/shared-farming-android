package com.pranisheba.sharedfarming.ui.activity
import android.annotation.SuppressLint
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.*
import android.os.Bundle
import android.text.InputType
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.button.MaterialButton
import com.google.android.material.tabs.TabLayoutMediator
import com.google.android.material.textfield.TextInputLayout
import com.pranisheba.sharedfarming.R
import com.pranisheba.sharedfarming.model.FundOpportunity
import com.pranisheba.sharedfarming.preference.SharedFarmingPreference
import com.pranisheba.sharedfarming.ui.adapter.SectionPageAdapter
import com.pranisheba.sharedfarming.ui.viewmodel.FundDetailsViewModel
import com.squareup.picasso.Picasso
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.launch
import androidx.core.graphics.BlendModeColorFilterCompat
import androidx.core.graphics.BlendModeCompat
import androidx.core.view.ViewCompat
import com.google.android.material.appbar.AppBarLayout
import com.pranisheba.sharedfarming.databinding.ActivityFundDetailBinding
import com.pranisheba.sharedfarming.model.FAQCheckout
import com.pranisheba.sharedfarming.util.*

class FundDetailsActivity : AppCompatActivity() {
  private lateinit var fundDetailsViewModel: FundDetailsViewModel
  private lateinit var binding: ActivityFundDetailBinding
  private lateinit var preference: SharedFarmingPreference
  private lateinit var fundOpportunity: FundOpportunity
  private var faqList = ArrayList<FAQCheckout>()
  @SuppressLint("SetTextI18n", "ResourceAsColor")
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    binding = ActivityFundDetailBinding.inflate(layoutInflater)
    fundDetailsViewModel = ViewModelProvider(this)[FundDetailsViewModel::class.java]
    setContentView(binding.root)
    setSupportActionBar(binding.toolbar)
    supportActionBar?.setDisplayHomeAsUpEnabled(true)
    supportActionBar?.setDisplayShowHomeEnabled(true)
    val sectionsPagerAdapter = SectionPageAdapter( this.supportFragmentManager,lifecycle)
    binding.viewPager.adapter = sectionsPagerAdapter
    TabLayoutMediator(binding.tabs,binding.viewPager) {tab,position->
      when(position){
        0->{
          tab.text= "Details"
        }
        1->{
          tab.text="How It Works"
        }
        2->{
          tab.text="FAQ"
        }
      }
    }.attach()
    preference = SharedFarmingPreference(this)
    if(!isNetworkAvailable(this)){
      showNoInternetBar(binding.tabs)
    }
    fundOpportunity = intent.getParcelableExtra(FUND_OPPORTUNITY)!!
    supportActionBar?.title = fundOpportunity.name
    binding.collapsingToolbarLayout.title = intent.extras?.getString(fundOpportunity.name)

    binding.collapsingToolbarLayout.setContentScrimColor(Color.parseColor("#339445"))
    binding.appBar.addOnOffsetChangedListener(AppBarLayout.OnOffsetChangedListener { _, verticalOffset ->
      if ((binding.collapsingToolbarLayout.height + verticalOffset) < (2 * ViewCompat.getMinimumHeight(binding.collapsingToolbarLayout))) {
        binding.toolbar.navigationIcon?.colorFilter =
          BlendModeColorFilterCompat.createBlendModeColorFilterCompat(resources.getColor(R.color.white), BlendModeCompat.SRC_ATOP)
        binding.collapsingToolbarLayout.title = fundOpportunity.name
      } else {
        binding.toolbar.navigationIcon?.colorFilter =
        BlendModeColorFilterCompat.createBlendModeColorFilterCompat(resources.getColor(R.color.purple_500), BlendModeCompat.SRC_ATOP)
     //   binding.collapsingToolbarLayout.setExpandedTitleTextColor(ColorStateList.valueOf(Color.parseColor("#329345")))
        binding.collapsingToolbarLayout.title = ""
      }
    })

    lifecycleScope.launch {
      if(fundOpportunity.faq != null){
        fundDetailsViewModel.getFaq(fundOpportunity.faq!!)
      }
    }
    fundDetailsViewModel.faqCheckoutList.observe(this, {
      faqList.clear()
      faqList.addAll(it)
    })

    Picasso.get().load(fundOpportunity.image).placeholder(R.drawable.ic_baseline_image_24)
      .error(R.drawable.ic_baseline_broken_image_24).into(binding.fundImageView)
    //colorChange(fundOpportunity.image,binding.collapsingToolbarLayout)

    binding.bottomSheet.setOnClickListener {
     showBottomSheetDialog()
    }
    binding.buttonBuyNow.setOnClickListener {
      showBottomSheetDialog()
    }
    fundDetailsViewModel.paymentCheckout.observe(this, {
      showSnackBar(binding.toolbar,"Congratulations! Your Fund is Successful!")
      startActivity(Intent(this, MainActivity::class.java))
      finishAffinity()
    })
    fundDetailsViewModel.progress.observe(this, {
      if (it) {
        binding.animationView.visibility = View.VISIBLE
      } else {
        binding.animationView.visibility = View.GONE
      }
    })
  }
  private fun buyNow(unitLayout: TextInputLayout?) {
    val unit: Int
    try {
    unit = unitLayout?.editText?.text.toString().toInt()
    } catch (e: Exception) {
    unitLayout?.error = getString(R.string.unit_required)
      return
    }
    if (preference.getAuthToken()?.isEmpty() == true) {
      startActivity(Intent(this, LoginActivity::class.java))
    } else {
      val token = "Token " + preference.getAuthToken()
      fundDetailsViewModel.buy(this, unit, fundOpportunity, token)
    }
  }
  override fun onOptionsItemSelected(item: MenuItem): Boolean {
    // handle arrow click here
    if (item.itemId == android.R.id.home) {
      onBackPressed()
    }
    return super.onOptionsItemSelected(item)
  }
  companion object {
    const val TAG = "FundDetailsActivity"
  }
  fun getObject(): FundOpportunity {
    return fundOpportunity
  }
  fun getObjectList(): List<FAQCheckout>{
    return faqList
  }
  @SuppressLint("SetTextI18n")
  private fun showBottomSheetDialog() {
    val bottomSheetDialog = BottomSheetDialog(this)
    bottomSheetDialog.setContentView(R.layout.bottom_sheet_buy)
    val unitLayout = bottomSheetDialog.findViewById(R.id.unitLayout) as TextInputLayout?
    val unitTextView = bottomSheetDialog.findViewById(R.id.unitTextView)as AutoCompleteTextView?
    val buyNowButton = bottomSheetDialog.findViewById(R.id.buyNowButton) as MaterialButton?
    val available = bottomSheetDialog.findViewById(R.id.available) as TextView?
    val maximumChoose = bottomSheetDialog.findViewById(R.id.maximumChoose) as TextView?
    available?.text = "Available cow * : "+fundOpportunity.available_cow.toString()
    maximumChoose?.text = "Maximum purchase * : 4"
    var lastCowNumber: Int
    lastCowNumber = fundOpportunity.available_cow!!
     if(lastCowNumber > 4){
       lastCowNumber = 4
     }
     val listForAnimals = ArrayList<String>()
     if(lastCowNumber > 0)
     for (i in 1 ..lastCowNumber){
       listForAnimals.add(i.toString())
     }

    unitLayout?.setOnKeyListener(null)
    ArrayAdapter(this, android.R.layout.simple_list_item_1, listForAnimals)
      .also { adapter ->
        unitTextView?.setAdapter(adapter)
        unitTextView?.inputType = InputType.TYPE_NULL
      }
    buyNowButton?.setOnClickListener {
      buyNow(unitLayout)
    }
    bottomSheetDialog.show()
  }

//  private fun colorChange(image: String?, collapseToolbar: CollapsingToolbarLayout) {
//    R.color.support_text_color
//       Picasso.get()
//      .load(image)
//      .into(object : Target{
//        override fun onBitmapLoaded(bitVal: Bitmap?, from: Picasso.LoadedFrom?) {
//          if(bitVal != null)
//          {
//            bitVal.toString()
//          val vibrant = createPaletteSync(bitVal).vibrantSwatch
//                if (vibrant != null) {
//                  // If we have a vibrant color
//                  // update the title TextView
//                  collapseToolbar.setBackgroundColor(vibrant.rgb )
//                  collapseToolbar.setExpandedTitleColor((vibrant.rgb ))
//                }
//          }
//          else{
//            Log.d("check","Invalid Bitmap")
//          }
//        }
//        override fun onBitmapFailed(e: java.lang.Exception?, errorDrawable: Drawable?) {
//        }
//        override fun onPrepareLoad(placeHolderDrawable: Drawable?) {
//        }
//
//      })
//  }
//  // Generate palette synchronously and return it
//  fun createPaletteSync(bitmap: Bitmap): Palette = Palette.from(bitmap).generate()


}