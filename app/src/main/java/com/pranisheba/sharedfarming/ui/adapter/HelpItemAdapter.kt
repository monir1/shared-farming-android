package com.pranisheba.sharedfarming.ui.adapter
import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.pranisheba.sharedfarming.R
import com.pranisheba.sharedfarming.model.HelpListItem

class HelpItemAdapter (
  private val list: ArrayList<HelpListItem>,
) :
  RecyclerView.Adapter<HelpItemAdapter.MyViewHolder>() {
  class MyViewHolder(view: View) :
    RecyclerView.ViewHolder(view) {
    val textViewQuestion : TextView? = view.findViewById(R.id.headerText)
    val textViewAns : TextView? = view.findViewById(R.id.headerDescription)
  }

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
    val itemView: View = LayoutInflater
      .from(parent.context)
      .inflate(
        R.layout.help_item,
        parent,
        false
      )
    return MyViewHolder(itemView)
  }
  @SuppressLint("SetTextI18n")
  override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
    val listData = list[position]
    holder.textViewQuestion?.text = listData.itemText
    holder.textViewAns?.text = listData.itemDescription

  }

  override fun getItemCount(): Int {
    return list.size
  }

}