package com.pranisheba.sharedfarming.ui.fragment

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.view.animation.LayoutAnimationController
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.pranisheba.sharedfarming.R
import com.pranisheba.sharedfarming.databinding.FragmentOfferProductBinding
import com.pranisheba.sharedfarming.model.FundOpportunity
import com.pranisheba.sharedfarming.ui.activity.FundDetailsActivity
import com.pranisheba.sharedfarming.ui.adapter.ViewAllFundAdapter
import com.pranisheba.sharedfarming.ui.viewmodel.HomeViewModel
import com.pranisheba.sharedfarming.util.FUND_OPPORTUNITY
import com.pranisheba.sharedfarming.util.isNetworkAvailable
import com.pranisheba.sharedfarming.util.showMsg


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [OfferProductFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class OfferProductFragment : Fragment() {
  // TODO: Rename and change types of parameters
  private var param1: String? = null
  private var param2: String? = null
  private var _binding: FragmentOfferProductBinding? = null
  private lateinit var homeViewModel: HomeViewModel
  private var fundOpportunities = ArrayList<FundOpportunity>()
  private var fundOpportunitiesGeneral = ArrayList<FundOpportunity>()
  private var fundOpportunitiesShariah = ArrayList<FundOpportunity>()
  // This property is only valid between onCreateView and
  // onDestroyView.
  private val binding get() = _binding!!
  private val animationList = intArrayOf(
    R.anim.layout_animation_left_to_right
  )
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    arguments?.let {
      param1 = it.getString(ARG_PARAM1)
      param2 = it.getString(ARG_PARAM2)
    }
  }

  override fun onCreateView(
    inflater: LayoutInflater, container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View {
    // Inflate the layout for this fragment
    _binding = FragmentOfferProductBinding.inflate(inflater, container, false)
    val root: View = binding.root
    binding.topTabTool.title = (activity as AppCompatActivity?)?.supportActionBar?.title
    binding.topTabTool.setTitleTextColor(Color.WHITE)
    if(!isNetworkAvailable(requireContext())){
      showMsg(requireContext(),"No Internet connection")
    }
    homeViewModel = ViewModelProvider(this)[HomeViewModel::class.java]
    homeViewModel.fundOpportunities.observe(viewLifecycleOwner, {
      //setting recycler to horizontal scroll
      binding.recyclerView.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)


      it.let {
        fundOpportunities.clear()
        for(i in it.indices){
          if(it[i].is_active == true) {
            fundOpportunities.add(it[i])
          }
        }
        //setting adapter to recycler
        binding.recyclerView.adapter = ViewAllFundAdapter(fundOpportunities,::onFundItemClickAll)


        if( fundOpportunities.size != 0  ){
          val length = fundOpportunities.size
          if (length > 0) {
            fundOpportunitiesGeneral.clear()
            fundOpportunitiesShariah.clear()
            for (i in 0 until length.toInt()){
              if(fundOpportunities[i].category.equals("general")) {
                val value = fundOpportunities[i]
                fundOpportunitiesGeneral.add(value)
              } else if(fundOpportunities[i].category.equals("shariah")){
                val value = fundOpportunities[i]
                fundOpportunitiesShariah.add(value)
              }
            }
          }
        }
      }
    })
    homeViewModel.getFundOpportunities()
    homeViewModel.progress.observe(viewLifecycleOwner, {
      if (it) {
        binding.animationView.visibility = View.VISIBLE
      } else {
        binding.animationView.visibility = View.GONE
      }
    })
    allButtonClickEvent()

    return root
  }

  private fun onFundItemClickAll(position: Int){
    if(!isNetworkAvailable(requireContext())){
      showMsg(requireContext(),"No Internet connection")
      return
    }
    val intent = Intent(context, FundDetailsActivity::class.java)
    intent.putExtra(FUND_OPPORTUNITY, fundOpportunities[position])
    startActivity(intent)
  }
  private fun onFundGeneralItemClickAll(position: Int){
    if(!isNetworkAvailable(requireContext())){
      showMsg(requireContext(),"No Internet connection")
      return
    }
    val intent = Intent(context, FundDetailsActivity::class.java)
    intent.putExtra(FUND_OPPORTUNITY, fundOpportunitiesGeneral[position])
    startActivity(intent)
  }
  private fun onFundShariahItemClickAll(position: Int){
    if(!isNetworkAvailable(requireContext())){
      showMsg(requireContext(),"No Internet connection")
      return
    }
    val intent = Intent(context, FundDetailsActivity::class.java)
    intent.putExtra(FUND_OPPORTUNITY, fundOpportunitiesShariah[position])
    startActivity(intent)
  }
  @SuppressLint("NotifyDataSetChanged")
  private fun runAnimationAgain(recyclerViewAdapter : ViewAllFundAdapter) {
    val controller: LayoutAnimationController =
      AnimationUtils.loadLayoutAnimation(requireContext(), animationList[0])
    binding.recyclerView.layoutAnimation = controller
    recyclerViewAdapter.notifyDataSetChanged()
    binding.recyclerView.scheduleLayoutAnimation()
  }
  private fun allButtonClickEvent() {
    binding.butAll.setOnClickListener {
      allDisplay()
    }
    binding.butShariah.setOnClickListener {
      shariahDisplay()
    }
    binding.butRegular.setOnClickListener {
      regularDisplay()
    }

    binding.butAll.setOnCloseIconClickListener {
      binding.butAll.isChecked = false
      binding.butAll.isCloseIconVisible = false
      basicCancel()
    }
    binding.butRegular.setOnCloseIconClickListener {
      binding.butRegular.isChecked = false
      binding.butRegular.isCloseIconVisible = false
      basicCancel()
    }
    binding.butShariah.setOnCloseIconClickListener {
      binding.butShariah.isCloseIconVisible = false
      binding.butShariah.isChecked = false
      basicCancel()
    }
  }
  private fun allDisplay(){
    val adapter = ViewAllFundAdapter(fundOpportunities,::onFundItemClickAll)
    binding.recyclerView.adapter = adapter
    runAnimationAgain(adapter)
    binding.butAll.isChecked = true
    binding.butRegular.isChecked = false
    binding.butShariah.isChecked = false
    binding.butAll.isCloseIconVisible = true
    binding.butRegular.isCloseIconVisible = false
    binding.butShariah.isCloseIconVisible = false

    binding.butShariah.visibility = View.GONE
    binding.butRegular.visibility = View.GONE
    binding.butAll.visibility = View.VISIBLE
  }
  private fun regularDisplay(){
    val adapter = ViewAllFundAdapter(fundOpportunitiesGeneral,::onFundGeneralItemClickAll)
    binding.recyclerView.adapter = adapter
    runAnimationAgain(adapter)
    binding.butRegular.isChecked = true
    binding.butAll.isChecked = false
    binding.butShariah.isChecked = false
    binding.butRegular.isCloseIconVisible = true
    binding.butShariah.isCloseIconVisible = false
    binding.butAll.isCloseIconVisible = false

    binding.butShariah.visibility = View.GONE
    binding.butAll.visibility = View.GONE
    binding.butRegular.visibility = View.VISIBLE
  }
  private fun shariahDisplay(){
    val adapter = ViewAllFundAdapter(fundOpportunitiesShariah,::onFundShariahItemClickAll)
    binding.recyclerView.adapter = adapter
    runAnimationAgain(adapter)
    binding.butShariah.isChecked = true
    binding.butRegular.isChecked = false
    binding.butAll.isChecked = false
    binding.butShariah.isCloseIconVisible = true
    binding.butRegular.isCloseIconVisible = false
    binding.butAll.isCloseIconVisible = false

    binding.butAll.visibility = View.GONE
    binding.butRegular.visibility = View.GONE
    binding.butShariah.visibility = View.VISIBLE
  }
  private fun basicCancel(){
    val adapter = ViewAllFundAdapter(fundOpportunities,::onFundItemClickAll)
    binding.recyclerView.adapter = adapter
    runAnimationAgain(adapter)
    binding.butShariah.visibility = View.VISIBLE
    binding.butAll.visibility = View.VISIBLE
  //  binding.butRegular.visibility = View.VISIBLE
  }

  override fun onDestroyView() {
    super.onDestroyView()
    //_binding = null
  }

  companion object {
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment OfferProductFragment.
     */
    // TODO: Rename and change types and number of parameters
    @JvmStatic
    fun newInstance(param1: String, param2: String) =
      OfferProductFragment().apply {
        arguments = Bundle().apply {
          putString(ARG_PARAM1, param1)
          putString(ARG_PARAM2, param2)
        }
      }
  }
}