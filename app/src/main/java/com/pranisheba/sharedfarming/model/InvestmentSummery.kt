package com.pranisheba.sharedfarming.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class InvestmentSummery (
 var  finish_projects :	Int? ,
 var running_projects :	Int? ,
 var total_investment:	String? ,
 var  running_investment :String? ,
 var total_return :String? ,
 var approx_return :String? ,
 ) : Parcelable