package com.pranisheba.sharedfarming.ui.adapter
import android.annotation.SuppressLint
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.pranisheba.sharedfarming.R
import com.pranisheba.sharedfarming.model.Invoice
import com.squareup.picasso.Picasso
import kotlin.math.roundToInt

class FarmAdapter(
  private val list: List<Invoice>,
  private val onItemClicked: (position: Int) -> Unit
) :
  RecyclerView.Adapter<FarmAdapter.MyViewHolder>() {

  class MyViewHolder(view: View, private val onItemClicked: (position: Int) -> Unit) :
    RecyclerView.ViewHolder(view), View.OnClickListener {

    private val farmCard: CardView = view.findViewById(R.id.farmCardView)
    val image: ImageView = view.findViewById(R.id.image)
    val nameAmount: TextView = view.findViewById(R.id.nameAmount)
    val location: TextView = view.findViewById(R.id.location)
    val duration : TextView = view.findViewById(R.id.duration)
    val amountUnit : TextView = view.findViewById(R.id.amountUnit)
    val investTotal : TextView = view.findViewById(R.id.investTotal)


    init {
      farmCard.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
      onItemClicked(adapterPosition)
    }
  }

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
    val itemView: View = LayoutInflater
      .from(parent.context)
      .inflate(
        R.layout.recycler_item_farm,
        parent,
        false
      )
    return MyViewHolder(itemView, onItemClicked)
  }

  @SuppressLint("SetTextI18n")
  override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
    val listData = list[position]

    //Loading Image into view
    Picasso.get().load(listData.product?.image).placeholder(R.drawable.ic_baseline_image_24)
      .error(R.drawable.ic_baseline_broken_image_24).into(holder.image)
    holder.nameAmount.text = listData.product?.name + " " + "(${listData.amount?.toDouble()?.roundToInt()} /Unit)"
    holder.location.text = listData.product?.location?.name.toString()+"(${listData.product?.location?.bn_name})"
    holder.duration.text = String.format("Duration: %d" , listData.product?.duration) + " month"
    if(!listData.product?.shariah_profit_from.isNullOrEmpty() && !listData.product?.shariah_profit_to.isNullOrEmpty()){
    //  holder.sharRel.visibility = View.VISIBLE
    //  holder.percentProfit2.text =  "Max \n" + listData.product?.shariah_profit_to.toString()+ "%"
    }
    else{
      Log.d("test","test")
    //    holder.genRel.visibility = View.VISIBLE
    //  holder.percentProfit.text =  listData.product?.profit_percentage.toString() + " %"
    }

    holder.amountUnit.text = "Invest: "+listData.product?.amount?.roundToInt().toString() +" * " + listData.unit

    holder.investTotal.text = "৳ "+getTotalInvest(listData)+ "K"

  }

  override fun getItemCount(): Int {
    return list.size
  }

  private fun getTotalInvest(invoice : Invoice): String {
    val v : Double
    val i : Double
    return if(invoice.product?.amount != null &&  invoice.unit != null){
      v = invoice.unit!!.toDouble()
      i = invoice.product?.amount!!.toDouble()
      val l = (i*v)/1000.00
      val l1 = l.roundToInt()
      l1.toString()

    } else{
      ""
    }
  }

}