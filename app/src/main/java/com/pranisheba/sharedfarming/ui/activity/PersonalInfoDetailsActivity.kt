package com.pranisheba.sharedfarming.ui.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.pranisheba.sharedfarming.R
import com.pranisheba.sharedfarming.databinding.ActivityPersonalInfoDetailsBinding
import com.pranisheba.sharedfarming.model.ProfileModel
import com.pranisheba.sharedfarming.model.ProfilePersonalItem
import com.pranisheba.sharedfarming.preference.SharedFarmingPreference
import com.pranisheba.sharedfarming.ui.adapter.PersonalDetailsItemAdapter
import com.pranisheba.sharedfarming.ui.viewmodel.ProfileViewModel
import com.pranisheba.sharedfarming.util.PROFILE_CONTACT_BASE
import com.pranisheba.sharedfarming.util.PROFILE_INFO
import com.pranisheba.sharedfarming.util.showMsg
import com.squareup.picasso.Picasso

class PersonalInfoDetailsActivity : AppCompatActivity() {
  private var personalDetails: ProfileModel? = null
  private lateinit var binding: ActivityPersonalInfoDetailsBinding
  private var profileItem = ArrayList<ProfilePersonalItem>()
  private lateinit var viewModel: ProfileViewModel
  private lateinit var contactOrNot : String
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    binding = ActivityPersonalInfoDetailsBinding.inflate(layoutInflater)
    setContentView(binding.root)
    supportActionBar?.setDisplayHomeAsUpEnabled(true)
    supportActionBar?.setDisplayShowHomeEnabled(true)
    binding.edit.visibility = View.GONE
    contactOrNot = intent.getStringExtra(PROFILE_CONTACT_BASE)!!
    if(contactOrNot == "contact"){
      supportActionBar?.setTitle(R.string.contact_details)
    }
    else
    supportActionBar?.setTitle(R.string.personal_details)


    viewModel = ViewModelProvider(this)[ProfileViewModel::class.java]
    binding.profileRecycle.layoutManager =
      LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

    getProfileInfo()
    binding.edit.setOnClickListener {
      if (personalDetails != null) {
        val intent = Intent(this, ProfileEditActivity::class.java)
        intent.putExtra(PROFILE_INFO, personalDetails)
        intent.putExtra(PROFILE_CONTACT_BASE,contactOrNot)
        startActivity(intent)
      } else {
        showMsg(this, "Please wait for a moment")
      }
    }

    viewModel.progress.observe(this, {
      if (it) {
        binding.animationView.visibility = View.VISIBLE
      } else {
        binding.animationView.visibility = View.GONE
      }
    })
  }

  private fun getProfileInfo() {
    val token = "token " + SharedFarmingPreference(this).getAuthToken().toString()
    viewModel.getProfile(token)
    viewModel.userProfile.observe(this,  { profileObjectList ->
      Log.d("Profile Info", profileObjectList.toString())
      if (profileObjectList != null) {
        personalDetails = profileObjectList.get(0)
        val url = personalDetails!!.profile_img.toString()
        //Loading Image into view
        Picasso.get()
          .load(url)
          .placeholder(R.drawable.ic_baseline_person_24)
          .error(R.drawable.ic_baseline_person_24)
          .into(binding.profileImage)

        getListData()
      } else {
        Log.d("Profile Info", "List of object is null")
      }

    })

  }

  override fun onResume() {
    super.onResume()
    getProfileInfo()
  }

  override fun onPause() {
    super.onPause()
  }

  private fun getListData() {
    binding.name.text = personalDetails?.name
    binding.mail.text = SharedFarmingPreference(this).getAuthMail()
    binding.phone.text = SharedFarmingPreference(this).getAuthPhone()
    profileItem.clear()
    if(contactOrNot == "contact"){
      binding.edit.visibility = View.GONE
      profileItem.add(ProfilePersonalItem("Address line 1", personalDetails?.address1))
      profileItem.add(ProfilePersonalItem("Address line 2", personalDetails?.address2))
      profileItem.add(ProfilePersonalItem("ZIP / Postal code", personalDetails?.zip_code))
      profileItem.add(ProfilePersonalItem("City", personalDetails?.city))
      profileItem.add(ProfilePersonalItem("Country", personalDetails?.country))
      binding.profileRecycle.adapter = PersonalDetailsItemAdapter(profileItem)
    }
    else{
      binding.edit.visibility = View.VISIBLE
      profileItem.add(ProfilePersonalItem("Date of Birth", personalDetails?.dob))
      profileItem.add(ProfilePersonalItem("Address line 1", personalDetails?.address1))
      profileItem.add(ProfilePersonalItem("Address line 2", personalDetails?.address2))
      profileItem.add(ProfilePersonalItem("National ID", personalDetails?.nid))
      profileItem.add(ProfilePersonalItem("ZIP / Postal code", personalDetails?.zip_code))
      profileItem.add(ProfilePersonalItem("City", personalDetails?.city))
      profileItem.add(ProfilePersonalItem("Country", personalDetails?.country))
      profileItem.add(ProfilePersonalItem("Company Name", personalDetails?.company_name))
      profileItem.add(ProfilePersonalItem("Occupation", personalDetails?.occupation))
      profileItem.add(ProfilePersonalItem("Nationality", personalDetails?.nationality))
      binding.profileRecycle.adapter = PersonalDetailsItemAdapter(profileItem)
    }


  }

  override fun onOptionsItemSelected(item: MenuItem): Boolean {
    // handle arrow click here
    if (item.itemId == android.R.id.home) {
      onBackPressed()
    }
    return super.onOptionsItemSelected(item)
  }

  companion object {
    const val TAG = "Personal Info Details Activity"
  }
}