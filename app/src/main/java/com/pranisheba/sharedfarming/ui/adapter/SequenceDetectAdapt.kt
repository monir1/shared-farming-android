package com.pranisheba.sharedfarming.ui.adapter
import com.pranisheba.sharedfarming.R
import com.transferwise.sequencelayout.SequenceAdapter
import com.transferwise.sequencelayout.SequenceStep

class SequenceDetectAdapt (private val items: List<FarmDetailItemVertically>)
  : SequenceAdapter<SequenceDetectAdapt.FarmDetailItemVertically>() {

  override fun getCount(): Int {
    return items.size
  }

  override fun getItem(position: Int): FarmDetailItemVertically {
    return items[position]
  }

  override fun bindView(sequenceStep: SequenceStep, item: FarmDetailItemVertically) {
    with(sequenceStep) {
      setActive(item.isActive)
      setAnchor(item.anchor)
      setAnchorTextAppearance(R.style.QText)
      setTitle(item.formattedDate)
      setTitleTextAppearance(R.style.QText)
      setSubtitle(item.title)
      setSubtitleTextAppearance(R.style.QText)

    }
  }

  data class FarmDetailItemVertically(val isActive: Boolean,
                                      val formattedDate: String,
                                      val title: String,
                                      val anchor : String)
}