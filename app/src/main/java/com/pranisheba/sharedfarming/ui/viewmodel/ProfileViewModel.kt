package com.pranisheba.sharedfarming.ui.viewmodel
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.pranisheba.sharedfarming.model.ImageUpload
import com.pranisheba.sharedfarming.model.PersonalDataSubmitModel
import com.pranisheba.sharedfarming.model.ProfileModel
import com.pranisheba.sharedfarming.networking.ApiClient
import com.pranisheba.sharedfarming.networking.ApiInterface
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.MultipartBody
import retrofit2.HttpException
import java.io.IOException

class ProfileViewModel : ViewModel() {
  val apiClient = ApiClient().getApiClient()?.create(ApiInterface::class.java)

  private val _progress = MutableLiveData<Boolean>()
  val progress: LiveData<Boolean>
    get() = _progress

  private val _userProf = MutableLiveData<MutableList<ProfileModel>>()
  val userProfile: LiveData<MutableList<ProfileModel>>
    get() = _userProf

  private val _tokenRemove = MutableLiveData<Boolean>()
  val tokenRemove: LiveData<Boolean>
    get() = _tokenRemove

   fun getProfile(token: String) {
    _progress.value = true
    _tokenRemove.value = false
    viewModelScope.launch {
      val response = try {
        withContext(Dispatchers.IO) {
          apiClient?.getProfileInfo(token)
        }
      } catch (e: IOException) {
        return@launch
      } catch (e: HttpException) {
        return@launch
      } catch (e: Exception) {
        return@launch
      }
      if (response?.isSuccessful == true && response.body() != null) {
        _userProf.value = response.body()
        _progress.value = false
      } else {
        _progress.value = false
        if (response?.code() == 401) {
          Log.d("doneValue :: ", response.message())
          _tokenRemove.value = true
        }
      }
    }
  }


  private val _userProfEdit = MutableLiveData<ProfileModel>()
  val userProfileEdit: LiveData<ProfileModel>
    get() = _userProfEdit


   fun submitProfileEdit(token: String, id: Int, obj: PersonalDataSubmitModel) {
    _progress.value = true
    _tokenRemove.value = false
    viewModelScope.launch {
      val response = try {
        withContext(Dispatchers.IO) {
          apiClient?.updatePersonalInfo(token, id, obj)
        }
      } catch (e: IOException) {
        return@launch
      } catch (e: HttpException) {
        return@launch
      } catch (e: Exception) {
        return@launch
      }
      if (response?.isSuccessful == true && response.body() != null) {
        _userProfEdit.value = response.body()

      } else {
        _progress.value = false
        if (response?.code() == 401) {
          Log.d("doneValue :: ", response.message())
          _tokenRemove.value = true
        }
      }
    }
  }
  private val _userProfEditImage = MutableLiveData<ProfileModel>()
  val userProfileEditImage: LiveData<ProfileModel>
    get() = _userProfEditImage
  fun uploadImage(token: String,id: Int,image : MultipartBody.Part){
    _progress.value = true
    _tokenRemove.value = false
    viewModelScope.launch {
      val response = try {
        withContext(Dispatchers.IO) {
          apiClient?.updatePicUser(token, id, image)
        }
      } catch (e: IOException) {
        return@launch
      } catch (e: HttpException) {
        return@launch
      } catch (e: Exception) {
        Log.d("tag",e.toString())
        return@launch
      }
      if (response?.isSuccessful == true && response.body() != null) {
        _userProfEditImage.value = response.body()
        _progress.value = false
      } else {
        _progress.value = false
        if (response?.code() == 401) {
          Log.d("doneValue :: ", response.message())
          _tokenRemove.value = true
        }
      }
    }
  }
}