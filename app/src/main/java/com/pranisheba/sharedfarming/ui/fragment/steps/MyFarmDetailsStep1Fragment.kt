package com.pranisheba.sharedfarming.ui.fragment.steps

import android.annotation.SuppressLint
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.lifecycle.lifecycleScope
import com.pranisheba.sharedfarming.R
import com.pranisheba.sharedfarming.databinding.FragmentMyFarmDetailsStep1Binding
import com.pranisheba.sharedfarming.model.Invoice
import com.pranisheba.sharedfarming.ui.activity.MyFarmDetailsActivity
import com.pranisheba.sharedfarming.util.isNetworkAvailable
import com.pranisheba.sharedfarming.util.showMsg

import com.squareup.picasso.Picasso
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.time.ZoneId
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter



// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [MyFarmDetailsStep1Fragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class MyFarmDetailsStep1Fragment : Fragment() {
  // TODO: Rename and change types of parameters
  private var param1: String? = null
  private var param2: String? = null
  private lateinit var binding: FragmentMyFarmDetailsStep1Binding
  private var invoice : Invoice? = null
  private var profitFrom : String ? = null
  private var profitTo : String? = null
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    arguments?.let {
      param1 = it.getString(ARG_PARAM1)
      param2 = it.getString(ARG_PARAM2)
    }
  }

  @RequiresApi(Build.VERSION_CODES.O)
  override fun onCreateView(
    inflater: LayoutInflater, container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View {
    // Inflate the layout for this fragment
    binding = FragmentMyFarmDetailsStep1Binding.inflate(inflater, container, false)
    binding.animationView.visibility = View.VISIBLE
    binding.containerView.visibility = View.GONE
    lifecycleScope.launch {
      delay(3000)
      invoice  = (activity as MyFarmDetailsActivity).getObject()
      Log.d("invoice object" , invoice.toString())
      setDataToView()
      binding.containerView.visibility = View.VISIBLE
    }
    if(!isNetworkAvailable(requireContext())){
      showMsg(requireContext(),"No Internet connection")
    }

    return binding.root
  }

  @RequiresApi(Build.VERSION_CODES.O)
  @SuppressLint("SetTextI18n", "SimpleDateFormat")
  private fun setDataToView() {
    binding.farmLocation.text =  invoice?.product?.location?.division?.name.toString() + " (${invoice?.product?.location?.name.toString()})"

    if(invoice?.amount?.toDouble()!! > 1000.00){
     val v = invoice?.amount?.toDouble()!!/1000
      binding.farmAmount.text ="Total : "+  v.toString()+"K ৳  "
    }
    else{
      binding.farmAmount.text ="Total : "+  invoice?.amount.toString()+" ৳  "
    }
    binding.avgWeight.text = "Avg. Weight : "+invoice?.product?.average_weight.toString() + " Kg"
    // example String
    val orderInitiatedDate = invoice?.timestamp.toString()
    // parse it to a ZonedDateTime and adjust the zone to the system default
    val localZonedDateTime = ZonedDateTime.parse(orderInitiatedDate)
      .withZoneSameInstant(ZoneId.systemDefault())

    binding.farmInitiated.text = "Investment date : "+ localZonedDateTime.format(
      DateTimeFormatter.ofPattern("EEE, MMM dd | HH:mm:ss a | yyyy")
    ).toString()


    binding.farmType.text =invoice?.product?.category.toString()
    binding.gender.text = "Gender : "+invoice?.product?.gender.toString()
    binding.farmGrowthTimeline.text = invoice?.product?.growth_timeline.toString()
    binding.breedName.text ="Breed : "+ invoice?.product?.breed?.name.toString()
    binding.breedSource.text = "Source : " + invoice?.product?.source.toString()
  //  binding.farmNumberOfCows.text = "Number of Cows : "+invoice?.product?.number_of_cows.toString()

    if(invoice?.product?.shariah_profit_from.toString() != "" && invoice?.product?.shariah_profit_to.toString() != ""){
      binding.farmProfitPercentage.text = "Profit : "+invoice?.product?.shariah_profit_from.toString()+" % to" +
          invoice?.product?.shariah_profit_to.toString()+" %"
      binding.roi.text = invoice?.product?.shariah_profit_from.toString()+" % to" +
          invoice?.product?.shariah_profit_to.toString()+" %"
      binding.amount.text = getAmountOfProfitShariah(invoice?.product?.shariah_profit_from.toString(), invoice?.product?.shariah_profit_to.toString())


    }
    else{
      binding.farmProfitPercentage.text = "Net Profit : "+invoice?.product?.profit_percentage.toString()+" %"
      binding.roi.text = invoice?.product?.profit_percentage.toString()+" %"
      binding.amount.text = getAmountOfProfit()
      binding.netProfitVal.text = getNetProfit()
    }
    binding.farmUnit.text = "Unit : "+invoice?.unit.toString()
    binding.farmInvest.text = "Invest : "+  "(${invoice?.unit} * ${invoice?.product?.amount})"


    binding.investPerCow.text = invoice?.product?.amount.toString()
    binding.qntOfCow.text = invoice?.unit.toString()
    binding.totalInvest.text = getTotalInvest()


    //Loading Image into view
    Picasso.get().load(invoice?.product?.image).placeholder(R.drawable.ic_baseline_image_24)
      .error(R.drawable.ic_baseline_broken_image_24).into(binding.farmImage)
    binding.animationView.visibility = View.GONE
  }

  private fun getNetProfit(): String {
    var v = ""
    if(getAmountOfProfit().isNotEmpty() && getTotalInvest().isNotEmpty()){
      v = (getTotalInvest().toDouble() + getAmountOfProfit().toDouble()).toString() + " ৳"
    }
    return v
  }

  private fun getAmountOfProfit(): String {
    var v = ""
    if(getTotalInvest().isNotEmpty() && invoice?.product?.profit_percentage != null){
     val vm =  (getTotalInvest().toDouble() * invoice?.product?.profit_percentage!!.toDouble())/100
      v = vm.toString()
    }
    return  v
  }

  private fun getShariahAmountOfProfit(profitFrom : String? , profitTo: String?) : String{
    var v = ""
    if(!profitFrom.isNullOrEmpty() || !profitTo.isNullOrEmpty()){
      val a =  getTotalInvest().toDouble() + profitFrom!!.toDouble()
      val b =  getTotalInvest().toDouble() + profitTo!!.toDouble()
      v = "$a to \n$b BDT"
    }

    return v
  }

  private fun getAmountOfProfitShariah(first : String? , rangeLast : String?): String {
    var v = ""
    if(getTotalInvest().isNotEmpty() && first != null){
      val vm =  (getTotalInvest().toDouble() * first.toDouble())/100
      profitFrom = vm.toString()
      if(getTotalInvest().isNotEmpty() && rangeLast != null){
        val vml =  (getTotalInvest().toDouble() * rangeLast.toDouble())/100
        v = "$vm to \n$vml"
        profitTo = vml.toString()
      }
      else{
        v = vm.toString()
      }

      if(profitFrom!=null && profitTo!= null){
        binding.netProfitVal.text = getShariahAmountOfProfit(profitFrom,profitTo)
      }
    }
    return  v
  }

  private fun getTotalInvest(): String {
    val v : Double
    val i : Double
    return if(invoice?.product?.amount != null &&  invoice?.unit != null){
      v = invoice?.unit!!.toDouble()
      i = invoice?.product?.amount!!.toDouble()
      (i * v).toString()
    } else{
      ""
    }
  }
  override fun onDestroyView() {
    super.onDestroyView()
  }

  companion object {
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MyFarmDetailsStep1Fragment.
     */
    // TODO: Rename and change types and number of parameters
    @JvmStatic
    fun newInstance(param1: String, param2: String) =
      MyFarmDetailsStep1Fragment().apply {
        arguments = Bundle().apply {
          putString(ARG_PARAM1, param1)
          putString(ARG_PARAM2, param2)
        }
      }
  }
}