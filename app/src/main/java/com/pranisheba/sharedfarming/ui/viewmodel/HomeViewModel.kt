package com.pranisheba.sharedfarming.ui.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.pranisheba.sharedfarming.model.BannerWhatsNew
import com.pranisheba.sharedfarming.model.FundOpportunity
import com.pranisheba.sharedfarming.networking.ApiClient
import com.pranisheba.sharedfarming.networking.ApiInterface
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import java.io.IOException

class HomeViewModel : ViewModel() {
  val apiClient = ApiClient().getApiClient()?.create(ApiInterface::class.java)

  private val _progress = MutableLiveData<Boolean>()
  val progress: LiveData<Boolean>
    get() = _progress

  private val _fundOpportunities = MutableLiveData<List<FundOpportunity>>()
  val fundOpportunities: LiveData<List<FundOpportunity>>
    get() = _fundOpportunities

  private val _fundOpportunity = MutableLiveData<FundOpportunity>()
  val fundOpportunity: LiveData<FundOpportunity>
    get() = _fundOpportunity

  private val _opportunitiesBanner = MutableLiveData<List<BannerWhatsNew>>()
  val opportunitiesBanner: LiveData<List<BannerWhatsNew>>
    get() = _opportunitiesBanner

  fun getFundOpportunities() {
    _progress.value = true
    viewModelScope.launch {

      val response = try {
        withContext(Dispatchers.IO) {
          apiClient?.getFundOpportunities()
        }
      } catch (e: IOException) {
        return@launch
      } catch (e: HttpException) {
        return@launch
      }
      //if doesn't match any..
      catch (e: Exception) {
        Log.d("error fund :: ", e.toString())
        return@launch
      }
      if (response?.isSuccessful == true && response.body() != null) {
        _fundOpportunities.value = response.body()
        _progress.value = false
      } else {
        _progress.value = false

      }
    }
  }

  fun getBannerWhatsNew() {
    _progress.value = true

    viewModelScope.launch {

      val response = try {
        withContext(Dispatchers.IO){
          apiClient?.bannerInfoWhatsNew()
        }

      } catch (e: IOException) {
        return@launch
      } catch (e: HttpException) {
        return@launch
      }
      //if doesn't match any..
      catch (e: Exception) {
        Log.d("error banner :: ", e.toString())
        return@launch
      }
      if (response?.isSuccessful == true && response.body() != null) {
        _opportunitiesBanner.value = response.body()
        _progress.value = false
      } else {
        _progress.value = false

      }
    }
  }

  fun getFundOpportunity(id : Int) {
    _progress.value = true
    viewModelScope.launch {
      val response = try {
        withContext(Dispatchers.IO){
          apiClient?.getOpportunitySingle(id)
        }
      } catch (e: IOException) {
        return@launch
      } catch (e: HttpException) {
        return@launch
      }
      //if doesn't match any..
      catch (e: Exception) {
        Log.d("error fund :: ", e.toString())
        return@launch
      }
      if (response?.isSuccessful == true && response.body() != null) {
        _fundOpportunity.value = response.body()
        _progress.value = false
      } else {
        _progress.value = false

      }
    }
  }
}
