package com.pranisheba.sharedfarming.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class BankingModel(
  var id : Int?,
  var name : String?,
  var branch_name : String?,
  var account_no : String?,
  var Account_name: String?,
) : Parcelable
