package com.pranisheba.sharedfarming.model

import okhttp3.MultipartBody

data class ImageUpload (
  var profile_image : MultipartBody.Part
    )