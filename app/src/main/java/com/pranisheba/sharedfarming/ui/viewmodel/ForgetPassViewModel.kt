package com.pranisheba.sharedfarming.ui.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.gson.JsonObject
import com.pranisheba.sharedfarming.model.ForgetPassPhoneEmailModel
import com.pranisheba.sharedfarming.model.ForgetPassword
import com.pranisheba.sharedfarming.model.SubmitToResetPass
import com.pranisheba.sharedfarming.networking.ApiClient
import com.pranisheba.sharedfarming.networking.ApiInterface
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import java.io.IOException


class ForgetPassViewModel : ViewModel() {
  val apiClient = ApiClient().getApiClient()?.create(ApiInterface::class.java)

  private val _progress = MutableLiveData<Boolean>()
  val progress: LiveData<Boolean>
    get() = _progress

  private val _forgetPass = MutableLiveData<ForgetPassword>()
  val forgetPass: LiveData<ForgetPassword>
    get() = _forgetPass

   fun getOTP(username: String) {
    _progress.value = true
    viewModelScope.launch {
      val response = try {
        withContext(Dispatchers.IO) {
          apiClient?.getForgetPasswordOTP(ForgetPassPhoneEmailModel(username))
        }
      } catch (e: IOException) {
        return@launch
      } catch (e: HttpException) {
        return@launch
      } catch (e: Exception) {
        return@launch
      }
      if (response?.isSuccessful == true && response.body() != null) {
        _forgetPass.value = response.body()
        _progress.value = false
      } else {
        _progress.value = false
        if (response?.code() == 400) {
          Log.d("Missing", "Unable to process with provided credentials")
        }
      }
    }
  }

  private val _passSubmitReport = MutableLiveData<JsonObject>()
  val passSubmitReport: LiveData<JsonObject>
    get() = _passSubmitReport

  fun resetPass(submitToResetPass: SubmitToResetPass) {
    _progress.value = true
    viewModelScope.launch {
      val response = try {
        withContext(Dispatchers.IO) {
          apiClient?.submitToResetNewPass(submitToResetPass)
        }
      } catch (e: IOException) {
        return@launch
      } catch (e: HttpException) {
        return@launch
      } catch (e: Exception) {
        return@launch
      }
      if (response?.isSuccessful == true && response.body() != null) {
        _passSubmitReport.value = response.body()
        _progress.value = false
      } else {
        _progress.value = false
        if (response?.code() == 400) {
          Log.d("Missing", "Unable to process ")
        }
      }
    }
  }


}