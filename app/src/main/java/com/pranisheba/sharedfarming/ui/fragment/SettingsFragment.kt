package com.pranisheba.sharedfarming.ui.fragment

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.pranisheba.sharedfarming.R
import com.pranisheba.sharedfarming.databinding.FragmentSettingsBinding
import com.pranisheba.sharedfarming.preference.SharedFarmingPreference
import com.pranisheba.sharedfarming.ui.activity.ForgetPasswordActivity
import com.pranisheba.sharedfarming.ui.viewmodel.SettingsViewModel
import com.pranisheba.sharedfarming.ui.activity.LoginActivity
import com.pranisheba.sharedfarming.ui.activity.MainActivity
import com.pranisheba.sharedfarming.ui.activity.ProfileActivity
import android.view.WindowInsets
import android.os.Build
import android.graphics.Insets
import com.pranisheba.sharedfarming.util.isNetworkAvailable
import com.pranisheba.sharedfarming.util.showMsg


class SettingsFragment : Fragment() {

  private lateinit var settingsViewModel: SettingsViewModel
  private var _binding: FragmentSettingsBinding? = null
  private val binding get() = _binding!!

  private lateinit var preference: SharedFarmingPreference

  override fun onCreateView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View {
    settingsViewModel =
      ViewModelProvider(this)[SettingsViewModel::class.java]
    _binding = FragmentSettingsBinding.inflate(inflater, container, false)
    val root: View = binding.root
    if(!isNetworkAvailable(requireContext())){
      showMsg(requireContext(),"No Internet connection")

    }
    preference = SharedFarmingPreference(requireContext())

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
      val windowMetrics = activity!!.windowManager.currentWindowMetrics
      val insets: Insets = windowMetrics.windowInsets
        .getInsetsIgnoringVisibility(WindowInsets.Type.systemBars())
      val params = binding.videoView.layoutParams
      params.width = windowMetrics.bounds.width() - insets.left - insets.right
      params.height = windowMetrics.bounds.width() - insets.top - insets.bottom
      binding.videoView.layoutParams = params
      binding.videoViewContainer.clipToOutline = true
    }
    else{
      binding.videoViewContainer.clipToOutline = true
    }

    if (preference.getAuthToken()?.isEmpty() == true) {
      binding.profileCard.visibility = View.GONE
      binding.forgetPass.visibility = View.VISIBLE
      binding.logInOutTextView.text = getText(R.string.log_in)
      binding.logInOutCard.setOnClickListener {
          startActivity(Intent(context, LoginActivity::class.java))
      }

      binding.logInOut.setImageResource(R.drawable.ic_baseline_login_24)
    } else {
      binding.profileCard.visibility = View.VISIBLE
      binding.forgetPass.visibility = View.GONE
      binding.logInOutTextView.text = getText(R.string.log_out)
      binding.logInOutCard.setOnClickListener {
            preference.putAuthToken(null)
            startActivity(Intent(context, MainActivity::class.java))
           // activity?.finishAffinity()
      }

      binding.logInOut.setImageResource(R.drawable.ic_log_out)
    }

    binding.profileCard.setOnClickListener {
      if(!isNetworkAvailable(requireContext())){
        showMsg(requireContext(),"No Internet connection")
        return@setOnClickListener
      }
      startActivity(Intent(context, ProfileActivity::class.java))
    }

    binding.forgetPass.setOnClickListener {
      if(!isNetworkAvailable(requireContext())){
        showMsg(requireContext(),"No Internet connection")
        return@setOnClickListener
      }
      startActivity(Intent(context, ForgetPasswordActivity::class.java))
    }
    binding.blogCard.setOnClickListener {
      showMsg(requireContext(),"This feature is updating")
    }
    binding.newsCard.setOnClickListener {
      showMsg(requireContext(),"This feature is updating")
    }
    binding.aboutCard.setOnClickListener {
      showMsg(requireContext(),"This feature is updating")
    }


    loadVideo()
    return root
  }


  @SuppressLint("UseCompatLoadingForDrawables")
  private fun loadVideo() {
    val path =   "android.resource://com.pranisheba.sharedfarming/raw/banner_video"
    val playView = binding.videoView
    val uri = Uri.parse(path)
    playView.setVideoURI(uri)
    playView.start()
    playView.setOnCompletionListener {
      playView.start()
    }
  }

  override fun onDestroyView() {
    super.onDestroyView()
    //_binding = null
  }

  override fun onResume() {
    super.onResume()
    loadVideo()
    if (preference.getAuthToken()?.isEmpty() == true) {
      binding.profileCard.visibility = View.GONE
      binding.logInOutTextView.text = getText(R.string.log_in)
      binding.logInOutCard.setOnClickListener {
        startActivity(Intent(context, LoginActivity::class.java))
      }
      binding.forgetPass.visibility = View.VISIBLE
    }
    else {
      binding.profileCard.visibility = View.VISIBLE
      binding.logInOutTextView.text = getText(R.string.log_out)
      binding.logInOutCard.setOnClickListener {
         showSimpleAlert()
      }
      binding.forgetPass.visibility = View.GONE
    }
  }



  private fun showSimpleAlert() {
    AlertDialog.Builder(requireContext())
      .setTitle("Log Out")
      .setMessage("Do you really want to log out ?")
      .setPositiveButton("Yes") { dialog, _ ->
        dialog.dismiss()
        preference.putAuthToken(null)
        startActivity(Intent(context, MainActivity::class.java))
        activity?.finishAffinity()
      }
      .setNegativeButton("No") { dialog, _ ->
        dialog.dismiss()
      }
      .setNeutralButton("Cancel") { dialog, _ ->
        dialog.dismiss()
      }
      .show()
  }



}