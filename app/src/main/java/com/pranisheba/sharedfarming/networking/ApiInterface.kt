package com.pranisheba.sharedfarming.networking

import com.google.gson.JsonObject
import com.pranisheba.sharedfarming.model.*
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.http.*

interface ApiInterface {

  //////////////////////  GET  /////////////////////////

  @GET("sharedfarm/fund_opportunities/")
  suspend fun getFundOpportunities(): Response<List<FundOpportunity>>

  @GET("sharedfarm/invoice/")
  suspend fun getInvoices(
    @Header("Authorization") token: String
  ): Response<List<Invoice>>

  @GET("sharedfarm/faq-item/")
  suspend fun getAllFAQ(@Query("category") category: Int): Response<MutableList<FAQCheckout>>

  @GET("auth/profile/")
  suspend fun getProfileInfo(@Header("Authorization") token: String): Response<MutableList<ProfileModel>>

  @GET("sharedfarm/invest_summery/")
  suspend fun getInvestSummery(@Header("Authorization") token: String): Response<InvestmentSummery>

  @GET("sharedfarm/whats_new/")
  suspend fun bannerInfoWhatsNew(): Response<List<BannerWhatsNew>>

//    @GET("sharedfarm/faq-item/{id}/")
//   suspend fun getSpecificFAQ( @Path("id") id: Int) : Response<FAQCheckout>

  //////////////////////  POST  /////////////////////////

  @POST("auth/signup/")
  suspend fun userSignUp(
    @Body userSignUp: UserSignUp
  ): Response<UserSignUp>

  @POST("auth/api-token-auth/")
  suspend fun userLogin(
    @Body userLogin: UserLogin
  ): Response<UserLogin>

  @POST("sharedfarm/sdk_checkout/")
  suspend fun checkout(
    @Header("Authorization") token: String,
    @Body paymentCheckout: PaymentCheckout
  ): Response<PaymentCheckout>


  @POST("auth/forgot-password/")
  suspend fun getForgetPasswordOTP(@Body emailOrPhone: ForgetPassPhoneEmailModel): Response<ForgetPassword>

  @POST("auth/confirm-password/")
  suspend fun submitToResetNewPass(@Body submitPassOtp: SubmitToResetPass): Response<JsonObject>

  //////////////////////  PUT  /////////////////////////

  @PUT("auth/profile/{id}/")
  suspend fun updatePersonalInfo(
    @Header("Authorization") token: String,
    @Path("id") id: Int,
    @Body updatedData: PersonalDataSubmitModel
  ): Response<ProfileModel>

  @Multipart
  @PATCH("auth/profile/{id}/")
  suspend fun updatePicUser(
    @Header("Authorization") token: String,
    @Path("id") id: Int,
    @Part  part : MultipartBody.Part
  ) : Response<ProfileModel>



  @GET("auth/bank/")
  suspend fun getBankInfo(@Header("Authorization") token: String) : Response<List<BankingModel>>

  @POST("auth/bank/")
  suspend fun postBankInfo(@Header("Authorization") token: String, @Body newData: PersonalBankingInfoSubmitModel): Response<BankingModel>


  @PATCH("auth/bank/{id}/")
  suspend fun updateBankInfo(@Header("Authorization") token: String, @Body newData: PersonalBankingInfoSubmitModel,@Path("id") id: Int ): Response<BankingModel>



  @GET("sharedfarm/fund_opportunities/{id}/")
  suspend fun getOpportunitySingle(@Path("id") id: Int) : Response<FundOpportunity>

  @GET("auth/nominee/")
  suspend fun getNomineeInfo(@Header("Authorization") token: String) : Response<List<NomineeModel>>

  @POST("auth/nominee/")
  suspend fun postNomineeInfo(@Header("Authorization") token: String, @Body newData: SubmitNomineeInfoModel): Response<NomineeModel>


  @PATCH("auth/nominee/{id}/")
  suspend fun updateNomineeInfo(@Header("Authorization") token: String, @Body newData: SubmitNomineeInfoModel,@Path("id") id: Int ): Response<NomineeModel>
}
