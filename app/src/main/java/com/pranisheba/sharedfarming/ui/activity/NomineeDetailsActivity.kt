package com.pranisheba.sharedfarming.ui.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.pranisheba.sharedfarming.R
import com.pranisheba.sharedfarming.databinding.ActivityNomineeDetailsBinding
import com.pranisheba.sharedfarming.model.NomineeModel
import com.pranisheba.sharedfarming.model.ProfileModel
import com.pranisheba.sharedfarming.model.ProfilePersonalItem
import com.pranisheba.sharedfarming.preference.SharedFarmingPreference
import com.pranisheba.sharedfarming.ui.adapter.PersonalDetailsItemAdapter
import com.pranisheba.sharedfarming.ui.viewmodel.NomineeViewModel
import com.pranisheba.sharedfarming.util.PROFILE_INFO
import com.pranisheba.sharedfarming.util.UPDATE_CREATE_NOMINEE
import com.pranisheba.sharedfarming.util.UPDATE_CREATE_NOMINEE_INFO
import com.pranisheba.sharedfarming.util.showMsg
import com.squareup.picasso.Picasso

class NomineeDetailsActivity : AppCompatActivity() {
  private lateinit var binding: ActivityNomineeDetailsBinding
  private lateinit var viewModel : NomineeViewModel
  private lateinit var personalDetails: ProfileModel
  private  var nomineeList = ArrayList<NomineeModel> ()
  private lateinit var nominee : NomineeModel
  private var nomineeItem = ArrayList<ProfilePersonalItem>()
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    binding = ActivityNomineeDetailsBinding.inflate(layoutInflater)
    setContentView(binding.root)
    viewModel = ViewModelProvider(this)[NomineeViewModel::class.java]
    supportActionBar?.setDisplayHomeAsUpEnabled(true)
    supportActionBar?.setDisplayShowHomeEnabled(true)
    supportActionBar?.setTitle(R.string.nominee_details)
    binding.edit.visibility = View.GONE
    binding.nomineeInfo.visibility = View.GONE
    binding.nomineeRecycle.visibility = View.GONE
    personalDetails = intent.getParcelableExtra(PROFILE_INFO)!!
    getCommonProfileData()
    binding.nomineeRecycle.layoutManager =
      LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
    getNomineeDetails()
    viewModel.progress.observe(this, {
      if (it) {
        binding.animationView.visibility = View.VISIBLE
      } else {
        binding.animationView.visibility = View.GONE
      }
    })

    viewModel.tokenRemove.observe(this, {
      if(it){
        showMsg(this, "Log In to view Info")
        SharedFarmingPreference(this@NomineeDetailsActivity).putAuthToken(null)
        startActivity(Intent(this@NomineeDetailsActivity,LoginActivity::class.java))
        finish()
      }
    })
  }

  private fun getNomineeDetails() {
    val token  = "token "+ SharedFarmingPreference(this@NomineeDetailsActivity).getAuthToken().toString()
    viewModel.getNominee(token)
    viewModel.nominee.observe(this,  {
      if(it != null){
        nomineeList = it as ArrayList<NomineeModel>
        nomineeItem.clear()

        if(nomineeList.size > 0){
          nominee = nomineeList[0]
//          binding.edit.text = getString(R.string.updateNom)
          binding.edit.visibility = View.VISIBLE
          binding.edit.setOnClickListener {
            val intent = Intent(this, CreateOrUpdateNomineeActivity::class.java)
            intent.putExtra(UPDATE_CREATE_NOMINEE_INFO, "update")
            intent.putExtra(UPDATE_CREATE_NOMINEE, nominee)
            intent.putExtra(PROFILE_INFO,personalDetails)
            startActivity(intent)
          }
          binding.nomineeRecycle.visibility = View.VISIBLE
          nomineeItem.add(ProfilePersonalItem("Name",nominee.name))
          nomineeItem.add(ProfilePersonalItem("Relationship",nominee.relationship))
          nomineeItem.add(ProfilePersonalItem("Contact No",nominee.contact_no))
          nomineeItem.add(ProfilePersonalItem("NID",nominee.nid))
          binding.nomineeRecycle.adapter = PersonalDetailsItemAdapter(nomineeItem)
        }
        else{
          binding.nomineeInfo.visibility = View.VISIBLE
//          binding.edit.text = getString(R.string.createNom)
          binding.edit.visibility = View.VISIBLE
          binding.edit.setOnClickListener {
            val intent = Intent(this, CreateOrUpdateNomineeActivity::class.java)
            intent.putExtra(UPDATE_CREATE_NOMINEE_INFO, "create")
            intent.putExtra(PROFILE_INFO,personalDetails)
            startActivity(intent)
          }
        }
      }
    })
  }

  private fun getCommonProfileData() {
    binding.name.text = personalDetails.name
    binding.mail.text = SharedFarmingPreference(this).getAuthMail()
    binding.phone.text = SharedFarmingPreference(this).getAuthPhone()
    val url = personalDetails.profile_img.toString()
    //Loading Image into view
    Picasso.get()
      .load(url)
      .placeholder(R.drawable.ic_baseline_person_24)
      .error(R.drawable.ic_baseline_person_24)
      .into(binding.profileImage)

  }

  override fun onOptionsItemSelected(item: MenuItem): Boolean {
    // handle arrow click here
    if (item.itemId == android.R.id.home) {
      onBackPressed()
    }
    return super.onOptionsItemSelected(item)
  }

  companion object {
    const val TAG = "Nominee Info Details Activity"
  }
}