package com.pranisheba.sharedfarming.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class ForgetPassword (
  var   user :	String?,
  var   details :	String?,
  var   email:String
) : Parcelable