package com.pranisheba.sharedfarming.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class SubmitToResetPass(
  var otp : String?,
  var new_password : String?
) : Parcelable
