package com.pranisheba.sharedfarming.ui.fragment

import android.content.Context
import android.os.Bundle

import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.pranisheba.sharedfarming.databinding.FragmentFAQBinding
import com.pranisheba.sharedfarming.model.FAQCheckout
import com.pranisheba.sharedfarming.ui.activity.FundDetailsActivity
import com.pranisheba.sharedfarming.ui.adapter.FaqAdapter
import com.pranisheba.sharedfarming.util.isNetworkAvailable
import com.pranisheba.sharedfarming.util.showMsg


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [FAQFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class FAQFragment : Fragment() {
  // TODO: Rename and change types of parameters
  private var param1: String? = null
  private var param2: String? = null
  private var _binding : FragmentFAQBinding? = null

  // This property is only valid between onCreateView and
  // onDestroyView.
  private val binding get() = _binding!!
  private var faqList = ArrayList<FAQCheckout>()
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    arguments?.let {
      param1 = it.getString(ARG_PARAM1)
      param2 = it.getString(ARG_PARAM2)
    }
  }

  override fun onCreateView(
    inflater: LayoutInflater, container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View {
    // Inflate the layout for this fragment
    _binding = FragmentFAQBinding.inflate(inflater, container, false)
    val root: View = binding.root
    if(!isNetworkAvailable(requireContext())){
      showMsg(requireContext(),"No Internet connection")
    }

    binding.faqRecycle.layoutManager =
      LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
      binding.faqRecycle.adapter = FaqAdapter(faqList)

    return root
  }
  override fun onAttach(activity: Context) {
    super.onAttach(activity)
    //this.fundAct = activity
    faqList  = (activity as FundDetailsActivity).getObjectList() as ArrayList<FAQCheckout>
  }

  override fun onDestroyView() {
    super.onDestroyView()
  }
  companion object {
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FAQFragment.
     */
    // TODO: Rename and change types and number of parameters
    @JvmStatic
    fun newInstance(param1: String, param2: String) =
      FAQFragment().apply {
        arguments = Bundle().apply {
          putString(ARG_PARAM1, param1)
          putString(ARG_PARAM2, param2)
        }
      }
  }
}