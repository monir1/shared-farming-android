package com.pranisheba.sharedfarming.ui.activity

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.animation.AnimationUtils
import androidx.recyclerview.widget.LinearLayoutManager
import com.pranisheba.sharedfarming.R
import com.pranisheba.sharedfarming.databinding.ActivityAllFarms2Binding
import com.pranisheba.sharedfarming.model.FundOpportunity
import com.pranisheba.sharedfarming.ui.adapter.ViewAllFundAdapter
import com.pranisheba.sharedfarming.util.*
import android.view.animation.LayoutAnimationController

class AllFarmsActivity2 : AppCompatActivity() {
  private lateinit var binding: ActivityAllFarms2Binding
  private var fundOpportunities = ArrayList<FundOpportunity>()
  private var fundOpportunitiesGeneral = ArrayList<FundOpportunity>()
  private var fundOpportunitiesShariah = ArrayList<FundOpportunity>()
  private var whatToLoad = 0
  private val animationList = intArrayOf(
    R.anim.layout_animation_left_to_right
  )

  @SuppressLint("NotifyDataSetChanged")
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    binding = ActivityAllFarms2Binding.inflate(layoutInflater)
    setContentView(binding.root)
    supportActionBar?.setDisplayHomeAsUpEnabled(true)
    supportActionBar?.setDisplayShowHomeEnabled(true)
    supportActionBar?.setTitle(R.string.fund_opportunities)
    binding.recyclerView.layoutManager =  LinearLayoutManager(this
      , LinearLayoutManager.VERTICAL, false)

    fundOpportunities = (intent.getSerializableExtra(ALL_FUND_OPPORTUNITY) as ArrayList<FundOpportunity>?)!!
    fundOpportunitiesGeneral = (intent.getSerializableExtra(REGULAR_OPPORTUNITY) as ArrayList<FundOpportunity>?)!!
    fundOpportunitiesShariah = (intent.getSerializableExtra(SHARIAH_OPPORTUNITY) as ArrayList<FundOpportunity>?)!!
    whatToLoad = intent.getIntExtra(WHAT_TO_LOAD_FUND,0)


    if(!isNetworkAvailable(this)){
      showNoInternetBar(binding.topTab)
    }


    //setting adapter to recycler
    if(fundOpportunities.isEmpty()){
      Log.d("List","List is null")
    }
    else{

      when (whatToLoad) {
        1 -> {
          Log.d("List",fundOpportunities.size.toString())
          allDisplay()

        }
        2 -> {
          Log.d("List",fundOpportunitiesGeneral.size.toString())
          regularDisplay()

        }
        3 -> {
          Log.d("List",fundOpportunitiesShariah.size.toString())
          shariahDisplay()
        }
        else ->
          showMsg(this,"Nothing to view")

      }

    }
    allButtonClickEvent()
  }


  private fun allButtonClickEvent() {
    binding.butAll.setOnClickListener {
      allDisplay()
    }
    binding.butShariah.setOnClickListener {
      shariahDisplay()
    }
    binding.butRegular.setOnClickListener {
      regularDisplay()
    }

    binding.butAll.setOnCloseIconClickListener {
      binding.butAll.isChecked = false
      binding.butAll.isCloseIconVisible = false
      basicCancel()
    }
    binding.butRegular.setOnCloseIconClickListener {
      binding.butRegular.isChecked = false
      binding.butRegular.isCloseIconVisible = false
      basicCancel()
    }
    binding.butShariah.setOnCloseIconClickListener {
      binding.butShariah.isCloseIconVisible = false
      binding.butShariah.isChecked = false
      basicCancel()
    }
  }

  private fun onFundItemClickAll(position: Int){
    if(!isNetworkAvailable(this)){
      showMsg(this,"No Internet connection")
      return
    }
    val intent = Intent(this, FundDetailsActivity::class.java)
    intent.putExtra(FUND_OPPORTUNITY, fundOpportunities[position])
    startActivity(intent)
  }
  private fun onFundGeneralItemClickAll(position: Int){
    if(!isNetworkAvailable(this)){
      showMsg(this,"No Internet connection")
      return
    }
    val intent = Intent(this, FundDetailsActivity::class.java)
    intent.putExtra(FUND_OPPORTUNITY, fundOpportunitiesGeneral[position])
    startActivity(intent)
  }
  private fun onFundShariahItemClickAll(position: Int){
    if(!isNetworkAvailable(this)){
      showMsg(this,"No Internet connection")
      return
    }
    val intent = Intent(this, FundDetailsActivity::class.java)
    intent.putExtra(FUND_OPPORTUNITY, fundOpportunitiesShariah[position])
    startActivity(intent)
  }

  override fun onOptionsItemSelected(item: MenuItem): Boolean {
    // handle arrow click here
    if (item.itemId == android.R.id.home) {
      onBackPressed()
    }
    return super.onOptionsItemSelected(item)
  }

  @SuppressLint("NotifyDataSetChanged")
  private fun runAnimationAgain(recyclerViewAdapter : ViewAllFundAdapter) {
    val controller: LayoutAnimationController =
    AnimationUtils.loadLayoutAnimation(this, animationList[0])
    binding.recyclerView.layoutAnimation = controller
    recyclerViewAdapter.notifyDataSetChanged()
    binding.recyclerView.scheduleLayoutAnimation()
  }

  private fun allDisplay(){
    val adapter = ViewAllFundAdapter(fundOpportunities,::onFundItemClickAll)
    binding.recyclerView.adapter = adapter
    runAnimationAgain(adapter)
    binding.butAll.isChecked = true
    binding.butRegular.isChecked = false
    binding.butShariah.isChecked = false
    binding.butAll.isCloseIconVisible = true
    binding.butRegular.isCloseIconVisible = false
    binding.butShariah.isCloseIconVisible = false

    binding.butShariah.visibility = View.GONE
    binding.butRegular.visibility = View.GONE
    binding.butAll.visibility = View.VISIBLE
  }
  private fun regularDisplay(){
    val adapter = ViewAllFundAdapter(fundOpportunitiesGeneral,::onFundGeneralItemClickAll)
    binding.recyclerView.adapter = adapter
    runAnimationAgain(adapter)
    binding.butRegular.isChecked = true
    binding.butAll.isChecked = false
    binding.butShariah.isChecked = false
    binding.butRegular.isCloseIconVisible = true
    binding.butShariah.isCloseIconVisible = false
    binding.butAll.isCloseIconVisible = false

    binding.butShariah.visibility = View.GONE
    binding.butAll.visibility = View.GONE
    binding.butRegular.visibility = View.VISIBLE
  }
  private fun shariahDisplay(){
    val adapter = ViewAllFundAdapter(fundOpportunitiesShariah,::onFundShariahItemClickAll)
    binding.recyclerView.adapter = adapter
    runAnimationAgain(adapter)
    binding.butShariah.isChecked = true
    binding.butRegular.isChecked = false
    binding.butAll.isChecked = false
    binding.butShariah.isCloseIconVisible = true
    binding.butRegular.isCloseIconVisible = false
    binding.butAll.isCloseIconVisible = false

    binding.butAll.visibility = View.GONE
    binding.butRegular.visibility = View.GONE
    binding.butShariah.visibility = View.VISIBLE
  }
  private fun basicCancel(){
    val adapter = ViewAllFundAdapter(fundOpportunities,::onFundItemClickAll)
    binding.recyclerView.adapter = adapter
    runAnimationAgain(adapter)
    binding.butShariah.visibility = View.VISIBLE
    binding.butAll.visibility = View.VISIBLE
   // binding.butRegular.visibility = View.VISIBLE
  }
}