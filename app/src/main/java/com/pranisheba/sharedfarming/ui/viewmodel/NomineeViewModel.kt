package com.pranisheba.sharedfarming.ui.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.pranisheba.sharedfarming.model.NomineeModel
import com.pranisheba.sharedfarming.model.SubmitNomineeInfoModel
import com.pranisheba.sharedfarming.networking.ApiClient
import com.pranisheba.sharedfarming.networking.ApiInterface
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import java.io.IOException

class NomineeViewModel : ViewModel() {
  val apiClient = ApiClient().getApiClient()?.create(ApiInterface::class.java)

  private val _progress = MutableLiveData<Boolean>()
  val progress: LiveData<Boolean>
  get() = _progress

  private val _nominee = MutableLiveData<List<NomineeModel>>()
  val nominee: LiveData<List<NomineeModel>>
    get() = _nominee

  private val _postNominee = MutableLiveData<NomineeModel>()
  val postNominee: LiveData<NomineeModel>
    get() = _postNominee

  private val _updateNominee = MutableLiveData<NomineeModel>()
  val updateNominee: LiveData<NomineeModel>
    get() = _updateNominee

  private val _tokenRemove = MutableLiveData<Boolean>()
  val tokenRemove: LiveData<Boolean>
    get() = _tokenRemove


  fun getNominee(token : String){
    _tokenRemove.value = false
    _progress.value = true
    viewModelScope.launch {
      val response = try {
        withContext(Dispatchers.IO) {
          apiClient?.getNomineeInfo(token)
        }
      } catch (e: IOException) {
        return@launch
      } catch (e: HttpException) {
        return@launch
      } catch (e: Exception) {
        Log.d("tag",e.toString())
        return@launch
      }
      if (response?.isSuccessful == true && response.body() != null) {
        _nominee.value = response.body()
        _progress.value = false
      } else {
        _progress.value = false
        if (response?.code() == 401) {
          Log.d("doneValue :: ", response.message())
          _tokenRemove.value = true
        }
      }
    }
  }

  fun postNominee(token : String , nomineeInfo : SubmitNomineeInfoModel){
    _tokenRemove.value = false
    _progress.value = true
    viewModelScope.launch {

      val response = try {
        withContext(Dispatchers.IO) {
          apiClient?.postNomineeInfo(token, nomineeInfo)
        }
      } catch (e: IOException) {
        return@launch
      } catch (e: HttpException) {
        return@launch
      } catch (e: Exception) {
        Log.d("tag",e.toString())
        return@launch
      }
      if (response?.isSuccessful == true && response.body() != null) {
        _postNominee.value = response.body()
        _progress.value = false
      } else {
        _progress.value = false
        if (response?.code() == 401) {
          Log.d("doneValue :: ", response.message())
          _tokenRemove.value = true
        }
      }
    }
  }

  fun updateNominee(token : String , nomineeInfo : SubmitNomineeInfoModel,nomineeId : Int){
    _tokenRemove.value = false
    _progress.value = true
    viewModelScope.launch {
      val response = try {
        withContext(Dispatchers.IO) {
          apiClient?.updateNomineeInfo(token, nomineeInfo, nomineeId)
        }
      } catch (e: IOException) {
        return@launch
      } catch (e: HttpException) {
        return@launch
      } catch (e: Exception) {
        Log.d("tag",e.toString())
        return@launch
      }
      if (response?.isSuccessful == true && response.body() != null) {
        _postNominee.value = response.body()
        _progress.value = false
      } else {
        _progress.value = false
        if (response?.code() == 401) {
          Log.d("doneValue :: ", response.message())
          _tokenRemove.value = true
        }
      }
    }
  }
}