package com.pranisheba.sharedfarming.ui.adapter
import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.pranisheba.sharedfarming.R
import com.pranisheba.sharedfarming.model.ProfilePersonalItem

class PersonalDetailsItemAdapter (
  private val list: List<ProfilePersonalItem>,
) :
  RecyclerView.Adapter<PersonalDetailsItemAdapter.MyViewHolder>() {

  class MyViewHolder(view: View) :
    RecyclerView.ViewHolder(view) {
    val textViewItem : TextView? = view.findViewById(R.id.itemTitle)
    val textViewContext : TextView? = view.findViewById(R.id.itemDescription)
  }

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
    val itemView: View = LayoutInflater
      .from(parent.context)
      .inflate(
        R.layout.profile_info_recycle_item,
        parent,
        false
      )
    return MyViewHolder(itemView)
  }

  @SuppressLint("SetTextI18n")
  override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
    val listData = list[position]
    holder.textViewItem?.text = listData.item
    if(listData.textOfItem != null) holder.textViewContext?.text = listData.textOfItem
    else{
      holder.textViewContext?.text = "None"
    }
  }

  override fun getItemCount(): Int {
    return list.size
  }

}