package com.pranisheba.sharedfarming.ui.fragment

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView.OnScrollListener
import com.pranisheba.sharedfarming.databinding.FragmentMyFarmsBinding
import com.pranisheba.sharedfarming.model.Invoice
import com.pranisheba.sharedfarming.preference.SharedFarmingPreference
import com.pranisheba.sharedfarming.ui.activity.LoginActivity
import com.pranisheba.sharedfarming.ui.activity.MyFarmDetailsActivity
import com.pranisheba.sharedfarming.ui.adapter.FarmAdapter
import com.pranisheba.sharedfarming.ui.viewmodel.MyFarmsViewModel
import androidx.recyclerview.widget.RecyclerView
import com.pranisheba.sharedfarming.util.*


class MyFarmsFragment : Fragment() {

  private lateinit var myFarmsViewModel: MyFarmsViewModel
  private var _binding: FragmentMyFarmsBinding? = null
  private lateinit var preference: SharedFarmingPreference

  // This property is only valid between onCreateView and
  // onDestroyView.
  private val binding get() = _binding!!

  private var invoices: List<Invoice>? = null

  @SuppressLint("SetTextI18n")
  override fun onCreateView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View {
    myFarmsViewModel =
      ViewModelProvider(this)[MyFarmsViewModel::class.java]
    _binding = FragmentMyFarmsBinding.inflate(inflater, container, false)
    val root: View = binding.root
    binding.topTabTool.title = (activity as AppCompatActivity?)?.supportActionBar?.title
    binding.topTabTool.setTitleTextColor(Color.WHITE)
    if(!isNetworkAvailable(requireContext())){
      showMsg(requireContext(),"No Internet connection")
    }

    preference = SharedFarmingPreference(requireContext())
    binding.logIn.setOnClickListener {
      showMsg(requireContext(), "Please Wait")
      startActivity(Intent(context, LoginActivity::class.java))
    }
    if (preference.getAuthToken()?.isEmpty() == true) {
      showMsg(requireContext(), "Log In to view Info")
      startActivity(Intent(context, LoginActivity::class.java))
    } else {
      myFarmsViewModel.invoices.observe(viewLifecycleOwner,  {
        binding.recyclerView.layoutManager =
          LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)

        if (it != null) {
          if (it.isNotEmpty()) {
            invoices = it
            //setting adapter to recycler
            binding.recyclerView.adapter = FarmAdapter(it, ::onFarmItemClick)
          } else {
            showSnackBar(binding.middleLayout,"No farm found !")
          }
        }
      })
      myFarmsViewModel.invoicesListObj.observe(viewLifecycleOwner,{
        if(it != null){

          if(!it.total_investment.isNullOrEmpty() ){

            if(it.total_investment?.toDouble()!! > 1000.0 && it.total_investment?.toDouble()!! < 100000.0){
              val v = it.total_investment!!.toDouble() / 1000.0
              binding.totalInvestmentValue.text = "৳ $v K"
            }
           else if(it.total_investment?.toDouble()!! > 100000.0 && it.total_investment?.toDouble()!! < 1000000.0){
             val v = it.total_investment!!.toDouble() / 100000.0
              binding.totalInvestmentValue.text = "৳ $v L"
            }
          else  if(it.total_investment?.toDouble()!! > 1000000.0 && it.total_investment?.toDouble()!! < 10000000.0){
              val v = it.total_investment!!.toDouble() / 1000000.0
              binding.totalInvestmentValue.text = "৳ $v M"
            }
          else  if(it.total_investment?.toDouble()!! > 10000000.0 ){
              val v = it.total_investment!!.toDouble() / 10000000.0
              binding.totalInvestmentValue.text = "৳ $v Cr"
            }
            else{
              binding.totalInvestmentValue.text = "৳ " +it.total_investment.toString()
            }

          }
          binding.finishProjectValue.text = it.finish_projects.toString()
          binding.runningProjectValue.text = it.running_projects.toString()

          if(!it.running_investment.isNullOrEmpty() ){

            if(it.running_investment?.toDouble()!! > 1000.0 && it.running_investment?.toDouble()!! < 100000.0){
              val v = it.running_investment!!.toDouble() / 1000.0
              binding.runningInvestmentValue.text = "৳ $v K"
            }
            else if(it.running_investment?.toDouble()!! > 100000.0 && it.running_investment?.toDouble()!! < 1000000.0){
              val v = it.running_investment!!.toDouble() / 100000.0
              binding.runningInvestmentValue.text = "৳ $v L"
            }
            else  if(it.running_investment?.toDouble()!! > 1000000.0 && it.running_investment?.toDouble()!! < 10000000.0){
              val v = it.running_investment!!.toDouble() / 1000000.0
              binding.runningInvestmentValue.text = "৳ $v M"
            }
            else  if(it.running_investment?.toDouble()!! > 10000000.0 ){
              val v = it.running_investment!!.toDouble() / 10000000.0
              binding.runningInvestmentValue.text = "৳ $v Cr"
            }
            else{
              binding.runningInvestmentValue.text = "৳ " +it.running_investment.toString()
            }

          }
          if(!it.total_return.isNullOrEmpty()){
            if(it.total_return?.toDouble()!! > 1000.0 && it.total_return?.toDouble()!! < 100000.0){
              val v = it.total_return!!.toDouble() / 1000.0
              binding.totalReturnValue.text = "৳ $v K"
            }
            else if(it.total_return?.toDouble()!! > 100000.0 && it.total_return?.toDouble()!! < 1000000.0){
              val v = it.total_return!!.toDouble() / 100000.0
              binding.totalReturnValue.text = "৳ $v L"
            }
            else  if(it.total_return?.toDouble()!! > 1000000.0 && it.total_return?.toDouble()!! < 10000000.0){
              val v = it.total_return!!.toDouble() / 1000000.0
              binding.totalReturnValue.text = "৳ $v M"
            }
            else  if(it.total_return?.toDouble()!! > 10000000.0 ){
              val v = it.total_return!!.toDouble() / 10000000.0
              binding.totalReturnValue.text = "৳ $v Cr"
            }
            else{
              binding.totalReturnValue.text = "৳ " +it.total_return.toString()
            }
          }
          if(!it.approx_return.isNullOrEmpty() ){

            if(it.approx_return?.toDouble()!! > 1000.0 && it.approx_return?.toDouble()!! < 100000.0){
              val v = it.approx_return!!.toDouble() / 1000.0
              binding.approxReturnValue.text = "৳ $v K"
            }
            else if(it.approx_return?.toDouble()!! > 100000.0 && it.approx_return?.toDouble()!! < 1000000.0){
              val v = it.approx_return!!.toDouble() / 100000.0
              binding.approxReturnValue.text = "৳ $v L"
            }
            else  if(it.approx_return?.toDouble()!! > 1000000.0 && it.approx_return?.toDouble()!! < 10000000.0){
              val v = it.approx_return!!.toDouble() / 1000000.0
              binding.approxReturnValue.text = "৳ $v M"
            }
            else  if(it.approx_return?.toDouble()!! > 10000000.0 ){
              val v = it.approx_return!!.toDouble() / 10000000.0
              binding.approxReturnValue.text = "৳ $v Cr"
            }
            else{
              binding.approxReturnValue.text = "৳ " +it.approx_return.toString()
            }

          }
        }
      })
      myFarmsViewModel.progress.observe(viewLifecycleOwner, {
        if (it) {
          binding.animationView.visibility = View.VISIBLE
        } else {
          binding.animationView.visibility = View.GONE
        }
      })

      myFarmsViewModel.tokenRemove.observe(viewLifecycleOwner,  {
        if(it){
          preference.putAuthToken(null)
          showMsg(requireContext(), "Log In to view your fund")
          startActivity(Intent(context, LoginActivity::class.java))
        }
      })
    }




    myFarmsViewModel.getInvoices("Token " + preference.getAuthToken())

    myFarmsViewModel.getInvoicesItemObj("Token " + preference.getAuthToken())

    binding.swipe.setOnRefreshListener {
      binding.swipe.isRefreshing = false
      myFarmsViewModel.getInvoices("Token " + preference.getAuthToken())
      myFarmsViewModel.getInvoicesItemObj("Token " + preference.getAuthToken())
    }


    controlRecycleView()

    return root
  }

  private fun onFarmItemClick(position: Int) {
  //  Toast.makeText(context, position.toString(), Toast.LENGTH_SHORT).show()
    if(!isNetworkAvailable(requireContext())){
      showMsg(requireContext(),"No Internet connection")
      return
    }
    val intent = Intent(context, MyFarmDetailsActivity::class.java)
    intent.putExtra(INVOICE, invoices?.get(position))
    startActivity(intent)


  }

  override fun onDestroyView() {
    super.onDestroyView()
    //_binding = null
  }

  override fun onResume() {
    super.onResume()
    if (preference.getAuthToken()?.isEmpty() == true) {
      showMsg(requireContext(), "Log In to view your fund")
      binding.scrool.visibility = View.GONE
      binding.swipe.visibility = View.GONE
      binding.dialogLogin.visibility = View.VISIBLE
    }
    else{
      binding.scrool.visibility = View.VISIBLE
      binding.dialogLogin.visibility = View.GONE
      binding.swipe.visibility = View.VISIBLE
    }
  }
  companion object {
    const val TAG = "MyFarmsFragment"
  }

 fun controlRecycleView(){
   binding.recyclerView.addOnScrollListener(object : OnScrollListener() {
     override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
       super.onScrolled(recyclerView, dx, dy)
       val topRowVerticalPosition =
         if (recyclerView.childCount == 0) 0 else recyclerView.getChildAt(0).top
          binding.swipe.isEnabled = topRowVerticalPosition >= 0
     }

     override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
       super.onScrollStateChanged(recyclerView, newState)
     }
   })
 }
}